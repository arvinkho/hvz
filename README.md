## Background

This project was the final assignment in the Java Fullstack course at Noroff for Experis Academy candidates for the spring 2022 class.

The scope of this project was to create a game administration application to keep track of a game of humans vs zombies. Humans vs zombies is a game of tag where the zombies' goal is to tag and "infect" humans. Upon getting tagged, the human will then become a zombie. Read more about the concept on the humans vs zombies web page: https://humansvszombies.org/.


# Usage

This Java application represents the back-end of our project. For documentation, please read the swagger docs: https://hvz-java-backend.herokuapp.com/api/v1/docs/swagger-ui/index.html


# Group

The group consisted of 5 people:

* Arvin Khodabandeh
* Erik Loftesnes
* Håkon Holm Erstad
* Simen-André Fosse
* Trym Ellingsen
