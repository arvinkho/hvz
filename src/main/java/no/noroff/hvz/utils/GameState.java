package no.noroff.hvz.utils;

// Keeps track of game state
public enum GameState {
    REGISTRATION,
    IN_PROGRESS,
    COMPLETE
}
