package no.noroff.hvz.utils;

// Record of user info. Contains user id, permissions and jwt validity
public record UserInfo(String userId, boolean isAdmin, boolean validAudience, boolean validDomain) {
}
