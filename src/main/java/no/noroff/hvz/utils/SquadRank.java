package no.noroff.hvz.utils;

// Keeps track of different squad ranks. Currently contains leader or not
public enum SquadRank {
    LEADER,
    MEMBER
}
