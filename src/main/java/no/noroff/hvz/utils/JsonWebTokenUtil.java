package no.noroff.hvz.utils;

import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.JWTParser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

// Helper class for decoding a jwt, getting information like user id and permissions.
@Component
public class JsonWebTokenUtil {

    @Value("${auth0.audience}")
    private String audience;
    @Value("${spring.security.oauth2.resourceserver.jwt.issuer-uri}")
    private String issuer;

    /**
     * Get the subject of the jwt. The authHeader string is provided in the controller.
     * @param authHeader String containing Bearer followed by a valid jwt.
     * @return UserInfo object containing user_id, a boolean describing if user is admin or not, and two more
     * boolean values for authentication of user internally in the application.
     */
    public UserInfo getUserInfo(String authHeader) {

        // Regex for removing prefixes of token and user_ids
        String userIdPrefixSeparator = "\\|";
        String tokenPrefix = "[Bb]earer ";

        // Role based access control claim name and role / permission name for admin
        String roleClaimName = "permissions";
        String adminRole = "admin";

        // Remove Bearer from authHeader, leaving the actual jwt
        String token = authHeader.replaceFirst(tokenPrefix, "");
        try {
            JWTClaimsSet claims = JWTParser.parse(token).getJWTClaimsSet();
            // Remove <prefix>| from the subject string, leaving only the database userId
            // TODO: Could the string behind the | not be unique between authenticators?
            String userId = claims.getSubject().split(userIdPrefixSeparator)[1];
            return new UserInfo(userId,
                    claims.getStringListClaim(roleClaimName).contains(adminRole),
                    claims.getAudience().contains(audience),
                    claims.getIssuer().equals(issuer));
        } catch (Exception e) {
            // We should never get here. If the jwt is invalid we will never be authenticated to call
            // the rest api endpoints
            e.printStackTrace();
            return null;
        }
    }
}
