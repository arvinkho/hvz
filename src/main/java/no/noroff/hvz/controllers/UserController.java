package no.noroff.hvz.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.hvz.mappers.UserMapper;
import no.noroff.hvz.models.Game;
import no.noroff.hvz.models.Users;
import no.noroff.hvz.models.dtos.UserCreateDto;
import no.noroff.hvz.models.dtos.UserDto;
import no.noroff.hvz.services.GameService;
import no.noroff.hvz.services.UserService;
import no.noroff.hvz.utils.GameState;
import no.noroff.hvz.utils.JsonWebTokenUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/user")
public class UserController {
    private final UserService userService;
    private final UserMapper userMapper;
    private final GameService gameService;
    private final JsonWebTokenUtil tokenUtil;

    public UserController(UserService userService, UserMapper userMapper, GameService gameService, JsonWebTokenUtil tokenUtil) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.gameService = gameService;
        this.tokenUtil = tokenUtil;
    }

    /**
     * Gets a specific user based on their userId
     * @param id The id of the user we want to find.
     * @return The user with the correct id, 404 if no such user exists.
     */
    @Operation(summary = "Get specific user by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the user",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserDto.class))}),
            @ApiResponse(responseCode = "404", description = "User doesn't exist"),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session")
    })
    @GetMapping("{id}")
    // TODO: Make more secure. Only return if id matches token? Make existsById instead?
    public ResponseEntity<UserDto> getUserById(@PathVariable String id) {
        HttpStatus status = HttpStatus.OK;
        if (!userService.userExistsById(id))
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        UserDto user = userMapper.mapFromEntityToDto(userService.getUserById(id));
        return new ResponseEntity<>(user, status);
    }

    /**
     * Add a new user to the database. The user added is the one sending the request.
     * @param userCreateDto The body of the user to add. Only contains name and email. Id is gotten from the jwt
     * @return The new user
     */
    @Operation(summary = "Create a new user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "New user was created",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserDto.class))}),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session"),
            @ApiResponse(responseCode = "400", description = "User already exists in database")
    })
    @PostMapping
    public ResponseEntity<UserDto> createUser(@RequestBody UserCreateDto userCreateDto,
                                              @Parameter(hidden = true)
                                              @RequestHeader(name = "Authorization") String authHeader) {
        HttpStatus status = HttpStatus.CREATED;
        String userId = tokenUtil.getUserInfo(authHeader).userId();
        if (userService.existsById(userId))
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        Users user = userMapper.mapFromCreateDtoToEntity(userCreateDto);
        user.setUserId(userId);
        user = userService.saveUser(user);
        UserDto userDto = userMapper.mapFromEntityToDto(user);
        return new ResponseEntity<>(userDto, status);
    }

    /**
     * Adds a game to an existing user. The user making the request is the user the game is added to.
     * @param gameId game to add
     * @return the user with the new gameId
     */
    @Operation(summary = "Specify which game a user is in")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User was updated",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = UserDto.class))}),
            @ApiResponse(responseCode = "404", description = "User doesn't exist"),
            @ApiResponse(responseCode = "400", description = "Game has started, so user can't register"),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session")
    })
    @PutMapping
    public ResponseEntity<UserDto> setGame(@RequestBody Integer gameId, @Parameter(hidden = true) @RequestHeader(name = "Authorization") String authHeader) {
        HttpStatus status = HttpStatus.OK;
        String userId = tokenUtil.getUserInfo(authHeader).userId();
        Users user = userService.getUserById(userId);
        if (!gameService.existsById(gameId)) {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }
        Game game = gameService.getGameById(gameId);
        if (game.getGameState() == GameState.IN_PROGRESS) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(status);
        }
        user.setGame(game);
        user = userService.saveUser(user);
        UserDto userDto = userMapper.mapFromEntityToDto(user);
        return new ResponseEntity<>(userDto, status);
    }
}
