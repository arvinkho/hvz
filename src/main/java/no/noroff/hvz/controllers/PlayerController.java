package no.noroff.hvz.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.hvz.generator.BiteCodeGenerator;
import no.noroff.hvz.mappers.PlayerMapper;
import no.noroff.hvz.models.Player;
import no.noroff.hvz.models.Users;
import no.noroff.hvz.models.dtos.PlayerAdminDto;
import no.noroff.hvz.models.dtos.PlayerCreateDto;
import no.noroff.hvz.models.dtos.PlayerDto;
import no.noroff.hvz.services.SquadService;
import no.noroff.hvz.utils.JsonWebTokenUtil;
import no.noroff.hvz.services.GameService;
import no.noroff.hvz.services.PlayerService;
import no.noroff.hvz.services.UserService;
import no.noroff.hvz.utils.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/game")
public class PlayerController {
    @Autowired
    PlayerService playerService;
    @Autowired
    PlayerMapper playerMapper;
    @Autowired
    GameService gameService;
    @Autowired
    UserService userService;
    @Autowired
    JsonWebTokenUtil tokenUtil;
    @Autowired
    SquadService squadService;

    // Player Mappings

    /**
     * Get all players in the database
     * @return List of all players
     */
    @Operation(summary = "Get all players in database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found players",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = PlayerDto.class))}),
            @ApiResponse(responseCode = "204", description = "No players exist"),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session"),
            @ApiResponse(responseCode = "403",
                    description = "User is not an admin")
    })
    @GetMapping("player")
    // TODO ADMIN PRIVILEGES?
    @PreAuthorize("hasAuthority('admin')")
    public ResponseEntity<List<PlayerDto>> getAllPlayers(@Parameter(hidden = true) @RequestHeader(name = "Authorization") String authHeader) {
        HttpStatus status = HttpStatus.OK;
        List<Player> playerList = playerService.getAllPlayers();
        if(playerList.isEmpty()) {
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(status);
        }
        boolean isAdmin = tokenUtil.getUserInfo(authHeader).isAdmin();
        List<PlayerDto> playerDtoList = playerMapper.mapFromEntityToDtoList(playerList, isAdmin);
        return new ResponseEntity<>(playerDtoList, status);
    }

    /**
     * Gets a list of all the players in the game, by gameId.
     * @param gameId Id of wanted game.
     * @return ResponseEntity with a list of playerDTO objects and an OK status code,
     * if the game doesn't exist or the playerList is empty, will return a BAD_REQUEST status code.
     */
    @Operation(summary = "Get all players in a game")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found players",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = PlayerDto.class))}),
            @ApiResponse(responseCode = "204", description = "No players exist in game"),
            @ApiResponse(responseCode = "404", description = "Game does not exist"),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session")
    })
    @GetMapping("{gameId}/player")
    public ResponseEntity<List<PlayerDto>> getAllPlayersInGame(@PathVariable int gameId,
                                                         @Parameter(hidden = true) @RequestHeader(name = "Authorization") String authHeader) {
        HttpStatus status = HttpStatus.OK;
        if(!gameService.existsById(gameId)) {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }
        List<Player> playerList = playerService.getAllPlayersInGame(gameId);
        if(playerList.size() == 0) {
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(status);
        }
        boolean isAdmin = tokenUtil.getUserInfo(authHeader).isAdmin();
        List<PlayerDto> playerDtoList = playerMapper.mapFromEntityToDtoList(playerList, isAdmin);
        return new ResponseEntity<>(playerDtoList, status);
    }

    /**
     * Get all the players in a squad
     * @param gameId Id of the game the squad is in
     * @param squadId Id of the squad itself
     * @return List of all players in the squad
     */
    @Operation(summary = "Get all players in a squad")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found players",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = PlayerDto.class))}),
            @ApiResponse(responseCode = "204", description = "No players exist in squad"),
            @ApiResponse(responseCode = "404", description = "Squad or game does not exist"),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session")
    })
    @GetMapping("{gameId}/squad/{squadId}/player")
    public ResponseEntity<List<PlayerDto>> getAllPlayersInSquad(@PathVariable int gameId, @PathVariable int squadId,
                                                               @RequestHeader(name = "Authorization") String authHeader) {
        HttpStatus status = HttpStatus.OK;
        if(!squadService.existsById(squadId, gameId)) {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }
        List<Player> playerList = playerService.getAllPlayersInSquad(squadId);
        if(playerList.isEmpty()) {
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(status);
        }
        boolean isAdmin = tokenUtil.getUserInfo(authHeader).isAdmin();
        List<PlayerDto> playerDtoList = playerMapper.mapFromEntityToDtoList(playerList, isAdmin);
        return new ResponseEntity<>(playerDtoList, status);
    }


    /**
     * Gets an existing player, by game and playerId.
     * @param gameId Id of wanted game.
     * @param playerId Id of wanted player.
     * @return ResponseEntity with a playerDTO object and an OK status code,
     * if the game or player doesn't exist, will return a BAD_REQUEST status code.
     */
    @Operation(summary = "Get player by game and player id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found player",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = PlayerDto.class))}),
            @ApiResponse(responseCode = "404", description = "Player not found within given game"),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session")
    })
    @GetMapping("{gameId}/player/{playerId}")
    public ResponseEntity<PlayerDto> getPlayerByIdAndGame(@PathVariable int gameId,
                                                   @PathVariable int playerId,
                                                   @Parameter(hidden = true) @RequestHeader(name = "Authorization") String authHeader) {
        HttpStatus status = HttpStatus.OK;
        if(!playerService.existsByPlayerAndGameId(playerId, gameId)) {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }
        boolean isAdmin = tokenUtil.getUserInfo(authHeader).isAdmin();
        Player player = playerService.getPlayerById(playerId);
        PlayerDto playerDto = playerMapper.mapFromEntityToDto(player, isAdmin);
        return new ResponseEntity<>(playerDto, status);
    }

    /**
     * Register a new player, by gameId, and a playerCreateDTO object.
     * @param gameId Id of wanted game.
     * @param playerCreateDto An object to create a new player.
     * @param authHeader The authorization header. Contains Bearer and a jwt.
     * @return ResponseEntity with a newly created playerDTO object and a CREATED status code.
     * If the game doesn't exist, will return a BAD_REQUEST status code.
     */
    @Operation(summary = "Create a new player and add it to a game")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Player created successfully",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = PlayerDto.class))}),
            @ApiResponse(responseCode = "400", description = "Game doesn't exist, or user already registered to game"),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session"),
            @ApiResponse(responseCode = "404", description = "Game wasn't found")
    })
    @PostMapping("{gameId}/player")
    public ResponseEntity<PlayerDto> registerPlayer(@PathVariable int gameId, @RequestBody PlayerCreateDto playerCreateDto,@Parameter(hidden = true) @RequestHeader(name = "Authorization") String authHeader) {
        HttpStatus status;
        UserInfo userInfo = tokenUtil.getUserInfo(authHeader);
        if (!gameService.existsById(gameId))
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        // If player is already registered.
        if(playerService.existsByGameIdAndUserId(gameId, userInfo.userId())) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(status);
        }
        Player player = playerMapper.mapFromCreateDtoToEntity(playerCreateDto);
        // If the user isn't an admin, set their values to default.
        if (!userInfo.isAdmin()) {
            player.setHuman(true);
            player.setIsPatientZero(false);
        }
        player.setBiteCode(BiteCodeGenerator.generateBiteCode());
        player = playerService.registerPlayer(gameId, player, userInfo.userId());
        status = HttpStatus.CREATED;
        PlayerDto playerDto = playerMapper.mapFromEntityToDto(player);
        return new ResponseEntity<>(playerDto, status);
    }

    /**
     * Updates an existing player, by game and playerId, as well as an updated playerCreateDTO object.
     * @param gameId Id of wanted game.
     * @param playerId Id of the player to update.
     * @return ResponseEntity with an updated playerDTO and an OK status code,
     * if the game or player doesn't exist, will return a BAD_REQUEST status code.
     */
    @Operation(summary = "Update a player with new information")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Player updated successfully",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = PlayerDto.class))}),
            @ApiResponse(responseCode = "400", description = "Game or player not found"),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session"),
            @ApiResponse(responseCode = "403", description = "User doesn't have permission to change player")
    })
    @PutMapping ("{gameId}/player/{playerId}")
    @PreAuthorize("hasAuthority('update:player')")
    public ResponseEntity<PlayerDto> updatePlayer(@PathVariable int gameId, @PathVariable int playerId, @RequestBody boolean human) {
        HttpStatus status = HttpStatus.OK;
        if (!playerService.existsByPlayerAndGameId(playerId, gameId)) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(status);
        }
        Player player = playerService.getPlayerById(playerId);
        player.setHuman(human);
        player = playerService.updatePlayerInGame(player, playerId);
        PlayerDto playerDto = playerMapper.mapFromEntityToDto(player);
        return new ResponseEntity<>(playerDto, status);
    }

    /**
     * Deletes an existing player, by game and playerId.
     * @param gameId Id of wanted game.
     * @param playerId Id of player to delete.
     * @return ResponseEntity with a NOT_FOUND status code or a NO_CONTENT status code if the game and player exists.
     */
    @Operation(summary = "Delete a player")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Player deleted successfully"),
            @ApiResponse(responseCode = "404", description = "Player not found within game"),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session"),
            @ApiResponse(responseCode = "403", description = "User doesn't have permission to delete players")
    })
    @DeleteMapping("{gameId}/player/{playerId}")
    public ResponseEntity<PlayerDto> deletePlayer(@PathVariable int playerId, @PathVariable int gameId,
                                                  @Parameter(hidden = true) @RequestHeader(name = "Authorization") String authHeader) {
        HttpStatus status = HttpStatus.NOT_FOUND;
        if (playerService.existsByPlayerAndGameId(playerId, gameId)) {
            UserInfo userInfo = tokenUtil.getUserInfo(authHeader);
            Player player = userService.getUserById(userInfo.userId()).getPlayer();
            // If the user isn't admin, and the player is trying to delete someone other than themselves
            if (!userInfo.isAdmin() && player != playerService.getPlayerById(playerId))
                // give a FORBIDDEN response
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            playerService.deletePlayer(playerId);
            status = HttpStatus.NO_CONTENT;
        }

        return new ResponseEntity<>(status);
    }

    /**
     * Gets a player based on their id, independent of game
     * @param playerId Id of the player we want to get
     * @return The player with the corresponding id, error code if not found / otherwise invalid
     */
    @Operation(summary = "Get player independent of game")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found player",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = PlayerDto.class))}),
            @ApiResponse(responseCode = "404", description = "No players with given id found"),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session"),
            @ApiResponse(responseCode = "403", description = "User is trying to get a player that is not themself")
    })
    @GetMapping("/player/{playerId}")
    public ResponseEntity<PlayerDto> getPlayerById(@PathVariable int playerId, @Parameter(hidden = true) @RequestHeader(name = "Authorization") String authHeader) {
        HttpStatus status = HttpStatus.OK;
        if (!playerService.existsById(playerId)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        String userId = tokenUtil.getUserInfo(authHeader).userId();

        Player player = playerService.getPlayerById(playerId);
        Users user = userService.getUserById(userId);

        if (player != user.getPlayer())
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);

        return new ResponseEntity<>(playerMapper.mapFromEntityToDto(player), status);
    }

}
