package no.noroff.hvz.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.hvz.mappers.KillMapper;
import no.noroff.hvz.models.Kill;
import no.noroff.hvz.models.dtos.KillCreateDto;
import no.noroff.hvz.models.dtos.KillDto;
import no.noroff.hvz.services.GameService;
import no.noroff.hvz.services.KillService;
import no.noroff.hvz.services.UserService;
import no.noroff.hvz.utils.JsonWebTokenUtil;
import no.noroff.hvz.utils.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/game/{gameId}")
public class KillController {
    @Autowired
    KillService killService;
    @Autowired
    KillMapper killMapper;
    @Autowired
    GameService gameService;
    @Autowired
    UserService userService;
    @Autowired
    JsonWebTokenUtil tokenUtil;

    // Kill Mappings

    /**
     * Gets a list of all the kills of the game provided.
     * @param gameId Id of game to get the kills from.
     * @return ResponseEntity with a list of killDTO objects and an OK status code,
     * if the game doesn't exist or the list of kills is empty returns a BAD_REQUEST.
     */
    @Operation(summary = "Get all kills in a game")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found kills",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = KillDto.class))}),
            @ApiResponse(responseCode = "204", description = "No kills exist in game"),
            @ApiResponse(responseCode = "400", description = "Game doesn't exist"),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session")
    })
    @GetMapping("kill")
    public ResponseEntity<List<KillDto>> getAllKills(@PathVariable int gameId) {
        HttpStatus status = HttpStatus.OK;
        if(!gameService.existsById(gameId)) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(status);
        }
        List<Kill> killList = killService.getAllKills(gameId);
        if(killList.size() == 0) {
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(status);
        }
        List<KillDto> killDtoList = killMapper.mapFromEntityToDtoList(killList);
        return new ResponseEntity<>(killDtoList, status);
    }

    /**
     * Gets a kill by killId and gameId.
     * @param gameId Id of the wanted game.
     * @param killId Id of the wanted kill.
     * @return ResponseEntity with a killDTO object and an OK status code,
     * if the game or kill doesn't exist returns a NOT_FOUND.
     */
    @Operation(summary = "Get a kill by game and kill id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the kill",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = KillDto.class))}),
            @ApiResponse(responseCode = "404", description = "Kill not found within game"),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session")
    })
    @GetMapping("kill/{killId}")
    public ResponseEntity<KillDto> getKillById(@PathVariable int gameId, @PathVariable int killId) {
        HttpStatus status = HttpStatus.OK;
        if(!killService.existsById(killId, gameId)) {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }
        Kill kill = killService.getKillById(gameId, killId);
        KillDto killDto = killMapper.mapFromEntityToDto(kill);
        return new ResponseEntity<>(killDto, status);
    }

    /**
     * Creates a kill by the gameId and a killCreateDTO object, using the bite code provided in the DTO.
     * @param gameId Id of the wanted game.
     * @param killCreateDto An object to create a kill.
     * @return ResponseEntity with a newly created killDTO object and a CREATED status code, if the game isn't found,
     * returns a 404. If the biteCode provided is invalid returns a BAD_REQUEST.
     */
    @Operation(summary = "Create a new kill")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Kill created successfully",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = KillDto.class))}),
            @ApiResponse(responseCode = "400", description = "Bite code is invalid"),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session"),
            @ApiResponse(responseCode = "404", description = "Game wasn't found")
    })
    @PostMapping("kill")
    public ResponseEntity<KillDto> createKill(@PathVariable int gameId,
                                              @RequestBody KillCreateDto killCreateDto,
                                              @Parameter(hidden = true) @RequestHeader(name = "Authorization") String authHeader) {
        HttpStatus status;
        String userId = tokenUtil.getUserInfo(authHeader).userId();
        if (!gameService.existsById(gameId))
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        if(!killService.validBiteCode(killCreateDto.getBiteCode(), gameId) ||
                killService.existsByBiteCodeAndGameId(killCreateDto.getBiteCode(), gameId)) {
            status = HttpStatus.BAD_REQUEST; 
            return new ResponseEntity<>(status);
        }
        Kill kill = killMapper.mapFromCreateDtoToEntity(killCreateDto);
        kill = killService.createKill(gameId, kill, killCreateDto.getBiteCode(), userId);
        status = HttpStatus.CREATED;
        KillDto killDto = killMapper.mapFromEntityToDto(kill);
        return new ResponseEntity<>(killDto, status);
    }

    /**
     * Updates an existing kill, by the game and killId, as well as an updated killCreateDTO object. This can
     * only be done by administrators or the killer
     * @param gameId Id of the wanted game.
     * @param killId Id of the wanted kill.
     * @param killCreateDto An object to create a kill.
     * @return ResponseEntity with an updated killDTO and an OK status code,
     * if the game or kill doesn't exist returns a BAD_REQUEST.
     */
    @Operation(summary = "Update a kill with new info")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Kill updated successfully",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = KillDto.class))}),
            @ApiResponse(responseCode = "400", description = "Kill doesn't exist in the provided game"),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session"),
            @ApiResponse(responseCode = "403", description = "User isn't admin or killer")
    })
    @PutMapping("kill/{killId}")
    public ResponseEntity<KillDto> updateKill(@PathVariable int gameId,
                                              @PathVariable int killId, @RequestBody KillCreateDto killCreateDto,

                                              @Parameter(hidden = true) @RequestHeader(name = "Authorization") String authHeader) {
        UserInfo userInfo = tokenUtil.getUserInfo(authHeader);
        HttpStatus status = HttpStatus.OK;
        if(!killService.existsById(killId, gameId)) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(status);
        }
        // If the user isn't the killer or an admin
        if (!userInfo.isAdmin() &&
                killService.getKillById(gameId, killId).getKiller()
                        != userService.getUserById(userInfo.userId()).getPlayer()) {
            status = HttpStatus.FORBIDDEN;
            return new ResponseEntity<>(status);
        }
        Kill kill = killMapper.mapFromCreateDtoToEntity(killCreateDto);
        kill = killService.updateKill(gameId, killId, kill);
        KillDto killDto = killMapper.mapFromEntityToDto(kill);
        return new ResponseEntity<>(killDto, status);
    }

    /**
     * Deletes an existing kill.
     * @param gameId Id of game with kill in.
     * @param killId Id of kill to delete.
     * @return ResponseEntity with a NOT_FOUND status code or a NO_CONTENT status code if the game and kill exists.
     */
    @Operation(summary = "Delete a kill by game and kill id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Kill deleted successfully"),
            @ApiResponse(responseCode = "404", description = "Kill wasn't found"),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session")
    })
    @DeleteMapping("kill/{killId}")
    @PreAuthorize("hasAuthority('delete:kill')")
    public ResponseEntity<KillDto> deleteKill(@PathVariable int gameId, @PathVariable int killId) {
        HttpStatus status = HttpStatus.NOT_FOUND;
        if(killService.existsById(killId, gameId)) {
            killService.deleteKill(gameId, killId);
            status = HttpStatus.NO_CONTENT;
        }
        return new ResponseEntity<>(status);
    }
}
