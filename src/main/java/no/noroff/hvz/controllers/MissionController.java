package no.noroff.hvz.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.hvz.mappers.MissionMapper;
import no.noroff.hvz.models.Mission;
import no.noroff.hvz.models.Player;
import no.noroff.hvz.models.Users;
import no.noroff.hvz.models.dtos.MissionCreateDto;
import no.noroff.hvz.models.dtos.MissionDto;
import no.noroff.hvz.services.GameService;
import no.noroff.hvz.services.MissionService;
import no.noroff.hvz.services.UserService;
import no.noroff.hvz.utils.JsonWebTokenUtil;
import no.noroff.hvz.utils.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/game/{gameId}")
public class MissionController {
    @Autowired
    MissionService missionService;
    @Autowired
    MissionMapper missionMapper;
    @Autowired
    GameService gameService;
    @Autowired
    UserService userService;
    @Autowired
    JsonWebTokenUtil jsonWebTokenUtil;

    // Mission Mappings

    /**
     * Gets a list of missions in the game.
     * @param gameId Id of the wanted game.
     * @return ResponseEntity with a list of missionDTO objects and an OK status code,
     * if the game doesn't exist, will return BAD_REQUEST status code. If there are no
     */
    @Operation(summary = "Get a list of all missions available")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found missions",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MissionDto.class))}),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session"),
            @ApiResponse(responseCode = "404",
                    description = "Game to get missions from doesn't exist"),
            @ApiResponse(responseCode = "204", description = "There were no valid missions in the game")
    })
    @GetMapping("mission")
    public ResponseEntity<List<MissionDto>> getAllMissions(@PathVariable int gameId,
                                                           @Parameter(hidden = true)
                                                           @RequestHeader(name = "Authorization") String authHeader) {
        HttpStatus status = HttpStatus.OK;
        if(!gameService.existsById(gameId)) {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }
        List<Mission> missionList = missionService.getAllMissions(gameId);
        UserInfo userInfo = jsonWebTokenUtil.getUserInfo(authHeader);
        Users user = userService.getUserById(userInfo.userId());
        if (!userInfo.isAdmin()) {
            Player player = user.getPlayer();
            // If the user is not an admin, only get the missions of their own faction.
            if (player.isHuman())
                // If player is human, filter to only get the missions that are visible to humans
                missionList = missionList.stream().filter(mission -> mission.isHumanVisible()).toList();
            else
                // If player is not human, only get the missions that are visible to zombies
                missionList = missionList.stream().filter(mission -> mission.isZombieVisible()).toList();
        }
        if(missionList.size() == 0) {
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(status);
        }
        List<MissionDto> missionDtoList = missionMapper.mapFromEntityToDtoList(missionList);
        return new ResponseEntity<>(missionDtoList, status);
    }

    /**
     * Gets a mission by game and missionId.
     * @param gameId Id of wanted game.
     * @param missionId Id of wanted mission.
     * @return ResponseEntity with a missionDTO object and an OK status code,
     * if the game or mission doesn't exist, will return BAD_REQUEST status code.
     */
    @Operation(summary = "Get a mission by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found mission",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MissionDto.class))}),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session"),
            @ApiResponse(responseCode = "404",
                    description = "Game or mission doesn't exist"),
            @ApiResponse(responseCode = "403",
                    description = "Player is not the correct faction to see the specified mission")
    })
    @GetMapping("mission/{missionId}")
    // TODO: Check if the mission same state as player (f.ex human = true), 403 if not
    public ResponseEntity<MissionDto> getMissionById(@PathVariable int gameId, @PathVariable int missionId,
                                                     @Parameter(hidden = true)
                                                     @RequestHeader(name = "Authorization") String authHeader) {
        HttpStatus status = HttpStatus.OK;
        if(!missionService.existsById(missionId, gameId)) {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }
        UserInfo userInfo = jsonWebTokenUtil.getUserInfo(authHeader);
        Mission mission = missionService.getMissionById(gameId, missionId);
        // If the user is not an admin
        if (!userInfo.isAdmin()) {
            Player player = userService.getUserById(userInfo.userId()).getPlayer();
            // If the player is human but mission is not, or...
            if ((player.isHuman() && !mission.isHumanVisible()) ||
                    // If player is zombie but mission is not
                    (!player.isHuman() && !mission.isZombieVisible()))
                // then return forbidden
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        MissionDto missionDto = missionMapper.mapFromEntityToDto(mission);
        return new ResponseEntity<>(missionDto, status);
    }

    /**
     * Creates a mission, by the gameId and a missionCreateDTO object.
     * @param gameId Id of the wanted game.
     * @param missionCreateDto An object to create a mission.
     * @return ResponseEntity with a newly created missionDTO object and a CREATED status code,
     * if game doesn't exist returns a BAD_REQUEST status code.
     */
    @Operation(summary = "Create a new mission")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Mission was created successfully",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MissionDto.class))}),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session"),
            @ApiResponse(responseCode = "404",
                    description = "Game doesn't exist"),
            @ApiResponse(responseCode = "403",
                    description = "User doesn't have permission to create missions")
    })
    @PostMapping("mission")
    @PreAuthorize("hasAuthority('create:mission')")
    public ResponseEntity<MissionDto> createMission(@PathVariable int gameId, @RequestBody MissionCreateDto missionCreateDto) {
        HttpStatus status;
        if(!gameService.existsById(gameId)) {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }
        Mission mission = missionMapper.mapFromCreateDtoToEntity(missionCreateDto);
        System.out.println(mission.getNwLat() + ", " + mission.getNwLng() + ", " + mission.getSeLat() + ", " + mission.getSeLng());
        mission = missionService.createMission(gameId, mission);
        status = HttpStatus.CREATED;
        MissionDto missionDto = missionMapper.mapFromEntityToDto(mission);
        return new ResponseEntity<>(missionDto, status);
    }

    /**
     * Updates a mission, by the game and missionId, as well as an updated missionCreateDTO.
     * @param gameId Id of the wanted game.
     * @param missionId Id of the mission to update.
     * @param missionCreateDto An object to update an existing mission.
     * @return ResponseEntity with an updated missionDTO and an OK status code,
     * if game or mission doesn't exist returns a BAD_REQUEST status code.
     */
    @Operation(summary = "Update a mission with new data")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Mission was updated successfully",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MissionDto.class))}),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session"),
            @ApiResponse(responseCode = "400",
                    description = "Game or mission doesn't exist"),
            @ApiResponse(responseCode = "403",
                    description = "User doesn't have permission to update missions")
    })
    @PutMapping("mission/{missionId}")
    @PreAuthorize("hasAuthority('update:mission')")
    public ResponseEntity<MissionDto> updateMission(@PathVariable int gameId, @PathVariable int missionId, @RequestBody MissionCreateDto missionCreateDto) {
        HttpStatus status = HttpStatus.OK;
        if(!missionService.existsById(missionId, gameId)) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(status);
        }
        Mission mission = missionMapper.mapFromCreateDtoToEntity(missionCreateDto);
        mission = missionService.updateMission(gameId, missionId, mission);
        MissionDto missionDto = missionMapper.mapFromEntityToDto(mission);
        return new ResponseEntity<>(missionDto, status);
    }

    /**
     * Deletes an existing mission, by game and missionId.
     * @param gameId Id of wanted game.
     * @param missionId Id of mission to delete.
     * @return ResponseEntity with a NOT_FOUND status code or a NO_CONTENT status code if the game and mission exists.
     */
    @Operation(summary = "Create a new mission")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Mission was deleted successfully"),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session"),
            @ApiResponse(responseCode = "404",
                    description = "Game or mission doesn't exist"),
            @ApiResponse(responseCode = "403",
                    description = "User doesn't have permission to delete missions")
    })
    @DeleteMapping("mission/{missionId}")
    @PreAuthorize("hasAuthority('delete:mission')")
    public ResponseEntity<MissionDto> deleteMission(@PathVariable int gameId, @PathVariable int missionId) {
        HttpStatus status = HttpStatus.NOT_FOUND;
        if(missionService.existsById(missionId, gameId)) {
            missionService.deleteMission(gameId, missionId);
            status = HttpStatus.NO_CONTENT;
        }
        return new ResponseEntity<>(status);
    }
}
