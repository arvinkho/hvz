package no.noroff.hvz.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.hvz.mappers.SquadCheckinMapper;
import no.noroff.hvz.mappers.SquadMapper;
import no.noroff.hvz.mappers.SquadMemberMapper;
import no.noroff.hvz.models.*;
import no.noroff.hvz.models.dtos.*;
import no.noroff.hvz.services.*;
import no.noroff.hvz.utils.JsonWebTokenUtil;
import no.noroff.hvz.utils.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/game/{gameId}")
public class SquadController {
    @Autowired
    SquadService squadService;
    @Autowired
    SquadMapper squadMapper;
    @Autowired
    SquadMemberMapper squadMemberMapper;
    @Autowired
    SquadCheckinMapper squadCheckinMapper;
    @Autowired
    GameService gameService;

    @Autowired
    PlayerService playerService;

    @Autowired
    SquadCheckinService squadCheckinService;

    @Autowired
    SquadMemberService squadMemberService;

    @Autowired
    UserService userService;

    @Autowired
    JsonWebTokenUtil tokenUtil;

    // Squad Mappings

    /**
     * Gets a list of all the squads in the game.
     * @param gameId Id of the wanted game.
     * @return ResponseEntity with a list of squadDTO objects and an OK status code,
     * if game doesn't exist or squadList is empty, will return a BAD_REQUEST status code.
     */
    @Operation(summary = "Get all squads in a game")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found squads",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = SquadDto.class))}),
            @ApiResponse(responseCode = "204", description = "No squads exist"),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session")
    })
    @GetMapping("squad")
    public ResponseEntity<List<SquadDto>> getAllSquads(@PathVariable int gameId) {
        HttpStatus status = HttpStatus.OK;
        if(!gameService.existsById(gameId)) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(status);
        }
        List<Squad> squadList = squadService.getAllSquads(gameId);
        if(squadList.size() == 0) {
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(status);
        }
        List<SquadDto> squadDtoList = squadMapper.mapFromEntityToDtoList(squadList);
        return new ResponseEntity<>(squadDtoList, status);
    }

    /**
     * Gets a squad, by game and squadId.
     * @param gameId Id of wanted game.
     * @param squadId Id of wanted squad.
     * @return ResponseEntity with a squadDTO object and an OK status code,
     * if game and squad doesn't exist, will return a BAD_REQUEST status code.
     */
    @Operation(summary = "Get squad by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the squad",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = SquadDto.class))}),
            @ApiResponse(responseCode = "404", description = "No squad with given id exists in given game"),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session")
    })
    @GetMapping("squad/{squadId}")
    public ResponseEntity<SquadDto> getSquadById(@PathVariable int gameId, @PathVariable int squadId) {
        HttpStatus status = HttpStatus.OK;
        if(!squadService.existsById(squadId, gameId)) {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }
        Squad squad = squadService.getSquadById(gameId, squadId);
        SquadDto squadDto = squadMapper.mapFromEntityToDto(squad);
        return new ResponseEntity<>(squadDto, status);
    }

    /**
     * Get the squad a squad member is a part of
     * @param gameId Id the squad and squad member is in
     * @param squadMemberId Id of the squad member we want to get the squad of
     * @return The squad of the specified squad member
     */
    @Operation(summary = "Get the squad that the member specified by squadMemberId is part of")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found squad of member",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = SquadMemberDto.class))}),
            @ApiResponse(responseCode = "404", description = "Squad member doesn't exist in given game"),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session")
    })
    @GetMapping("squad/squadmember/{squadMemberId}")
    public ResponseEntity<SquadDto> getSquadBySquadMemberId(@PathVariable int gameId, @PathVariable int squadMemberId) {
        HttpStatus status = HttpStatus.OK;
        if(!squadMemberService.existsById(squadMemberId) && gameService.existsById(gameId)) {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }
        Squad squad = squadService.getSquadBySquadMemberId(squadMemberId);
        SquadDto squadDto = squadMapper.mapFromEntityToDto(squad);
        return new ResponseEntity<>(squadDto, status);
    }

    /**
     * Creates a squad, by gameId and a squadCreateDTO object. Also creates a SquadMember from the user who made
     * the request to set as squad leader
     * @param gameId Id of wanted game.
     * @param squadCreateDto An object to create a new squad.
     * @return ResponseEntity with a newly created squadDTO object and a CREATED status code,
     * if the game doesn't exist, or if the user is already in a squad, will return a BAD_REQUEST status code.
     */
    @Operation(summary = "Create a new squad")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Squad created successfully",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = SquadDto.class))}),
            @ApiResponse(responseCode = "400", description = "User is already in a squad"),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session"),
            @ApiResponse(responseCode = "404", description = "Game not found")
    })
    @PostMapping("squad")
    public ResponseEntity<SquadDto> createSquad(@PathVariable int gameId,
                                                @RequestBody SquadCreateDto squadCreateDto,
                                                @Parameter(hidden = true) @RequestHeader(name = "Authorization") String authHeader) {
        HttpStatus status;
        String userId = tokenUtil.getUserInfo(authHeader).userId();
        Player player = userService.getUserById(userId).getPlayer();
        Game game = gameService.getGameById(gameId);
        if (!gameService.existsById(gameId))
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        if(squadService.playerAlreadyInSquad(player, game)) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(status);
        }
        Squad squad = squadMapper.mapFromCreateDtoToEntity(squadCreateDto);
        squad = squadService.createSquad(game, squad, player);
        status = HttpStatus.CREATED;
        SquadDto squadDto = squadMapper.mapFromEntityToDto(squad);
        return new ResponseEntity<>(squadDto, status);
    }

    /**
     * Creates a squad member, by game and squadId, and a squadMemberCreateDto.
     * @param gameId Id of wanted game.
     * @param squadId Id of squad to add member to.
     * @param squadMemberCreateDto An object to create a squad member.
     * @return ResponseEntity with a newly created squadMemberDTO object and an OK status code,
     * if the game or squad doesn't exist, will return a BAD_REQUEST status code.
     */
    @Operation(summary = "Create a new squad member and add it to a squad")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Member created successfully",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = SquadMemberDto.class))}),
            @ApiResponse(responseCode = "400",
                    description = "Squad or game doesn't exist, or user is already in a squad"),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session"),
            @ApiResponse(responseCode = "404", description = "Game or squad not found")
    })
    @PostMapping("squad/{squadId}")
    public ResponseEntity<SquadMemberDto> createSquadMember(@PathVariable int gameId, @PathVariable int squadId,
                                                            @RequestBody SquadMemberCreateDto squadMemberCreateDto) {
        HttpStatus status = HttpStatus.CREATED;
        if (!squadService.existsById(squadId, gameId))
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        if(playerService.getPlayerById(squadMemberCreateDto.getPlayerId()).getSquadMember() != null) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(status);
        }
        SquadMember squadMember = squadMemberMapper.mapFromCreateDtoToEntity(squadMemberCreateDto);
        squadMember.setGame(gameService.getGameById(gameId));
        squadMember.setSquad(squadService.getSquadById(gameId, squadId));
        squadMember.setPlayer(playerService.getPlayerById(squadMemberCreateDto.getPlayerId()));

        squadMember = squadService.createSquadMember(gameId, squadId, squadMember);
        SquadMemberDto squadMemberDto = squadMemberMapper.mapFromEntityToDto(squadMember);
        return new ResponseEntity<>(squadMemberDto, status);
    }

    /**
     * Updates a squad, by game and squadId, as well as an updated squadCreateDTO.
     * @param gameId Id of wanted game.
     * @param squadId Id of squad to update.
     * @param squadCreateDto An object to update an existing squad.
     * @return ResponseEntity with an updated squadDTO object and an OK status code,
     * if game or squad doesn't exist, will return a BAD_REQUEST status code.
     */
    @Operation(summary = "Update a squad by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Squad successfully updated",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = SquadDto.class))}),
            @ApiResponse(responseCode = "400", description = "Game or squad doesn't exist"),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session"),
            @ApiResponse(responseCode = "403", description = "User doesn't have permission to update squad")
    })
    @PutMapping("squad/{squadId}")
    @PreAuthorize("hasAuthority('update:squad')")
    public ResponseEntity<SquadDto> updateSquad(@PathVariable int gameId, @PathVariable int squadId, @RequestBody SquadCreateDto squadCreateDto) {
        HttpStatus status = HttpStatus.OK;
        if(!squadService.existsById(squadId, gameId)) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(status);
        }
        Squad squad = squadMapper.mapFromCreateDtoToEntity(squadCreateDto);
        squad = squadService.updateSquad(gameId, squadId, squad);
        SquadDto squadDto = squadMapper.mapFromEntityToDto(squad);
        return new ResponseEntity<>(squadDto, status);
    }

    /**
     * Deletes an existing squad, by game and squadId.
     * @param gameId Id of wanted game.
     * @param squadId Id of squad to delete.
     * @return ResponseEntity with a NOT_FOUND status code or a NO_CONTENT status code if the game and squad exists.
     */
    @Operation(summary = "Delete a squad")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Squad deleted successfully"),
            @ApiResponse(responseCode = "404", description = "Squad not found in the given game"),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session"),
            @ApiResponse(responseCode = "403",
                    description = "User doesn't have permission to delete squad")
    })
    @DeleteMapping("squad/{squadId}")
    @PreAuthorize("hasAuthority('delete:squad')")
    public ResponseEntity<SquadDto> deleteSquad(@PathVariable int gameId, @PathVariable int squadId) {
        HttpStatus status = HttpStatus.NOT_FOUND;
        if(squadService.existsById(squadId, gameId)) {
            squadService.deleteSquad(gameId, squadId);
            status = HttpStatus.NO_CONTENT;
        }
        return new ResponseEntity<>(status);
    }

    /**
     * Delete a squad member from a squad and game
     * @param gameId Id of the game the squad and squad member is in
     * @param squadId Id of the squad the member is in
     * @param squadMemberId Id of the squad member to remove
     * @return 204 if successfully deleted, 404 if not found
     */
    @Operation(summary = "Remove a squad member from a squad and delete said member")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Squad member deleted successfully"),
            @ApiResponse(responseCode = "404", description = "No squad member found for the given squad"),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session")
    })
    @DeleteMapping("squad/{squadId}/squadmember/{squadMemberId}")
    public ResponseEntity<SquadDto> deleteSquadMember(@PathVariable int gameId,
                                                      @PathVariable(name = "squadId") int squadId,
                                                      @PathVariable(name = "squadMemberId") int squadMemberId) {
        HttpStatus status = HttpStatus.NOT_FOUND;
        if(squadService.existsById(squadId, gameId) && squadMemberService.existsById(squadMemberId)) {
            squadService.deleteSquadMember(squadMemberId, squadId);
            status = HttpStatus.NO_CONTENT;
        }
        return new ResponseEntity<>(status);
    }

    // Squad Check-In Mappings

    /**
     * Gets a list of squad check-in markers, by game and squadId.
     * @param gameId Id of wanted game.
     * @param squadId Id of squad to check for markers.
     * @return ResponseEntity with a list of squadCheckinDTO objects and an OK status code,
     * if the game or squad doesn't exist or the squadCheckinList is empty, will return a BAD_REQUEST status code.
     */
    @Operation(summary = "Get all checkin markers from squad")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found markers",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = SquadCheckinDto.class))}),
            @ApiResponse(responseCode = "404", description = "Squad is not found in the given game"),
            @ApiResponse(responseCode = "403", description = "User is not part of the same faction as the squad"),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session"),
            @ApiResponse(responseCode = "204", description = "No valid checkin markers found")
    })
    @GetMapping("squad/{squadId}/check-in")
    public ResponseEntity<List<SquadCheckinDto>> getSquadCheckinMarkers(@PathVariable int gameId,
                                                                        @PathVariable int squadId,
                                                                        @Parameter(hidden = true) @RequestHeader(name = "Authorization") String authHeader) {
        HttpStatus status = HttpStatus.OK;
        if(!squadService.existsById(squadId, gameId)) {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }

        UserInfo userInfo = tokenUtil.getUserInfo(authHeader);
        Player player = null;
        // Just in case the admin isn't registered as a user in the database, TODO: maybe remove?
        if (!userInfo.isAdmin())
            player = userService.getUserById(userInfo.userId()).getPlayer();
        Squad squad = squadService.getSquadById(gameId, squadId);

        // Check if player is still human. If not, send forbidden
        if (player != null && player.isHuman() != squad.isHuman())
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);

        // Get squad markers.
        List<SquadCheckin> squadCheckinList = squadService.getSquadCheckinMarkers(gameId, squad,
                player, userInfo.isAdmin());

        if(squadCheckinList.size() == 0) {
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(status);
        }
        List<SquadCheckinDto> squadCheckinDtoList = squadCheckinMapper.mapFromEntityToDtoList(squadCheckinList);
        return new ResponseEntity<>(squadCheckinDtoList, status);
    }

    /**
     * Creates a squad check-in marker, by game and squadId, and a squadCheckinCreateDTO object.
     * @param gameId Id of wanted game.
     * @param squadId Id of squad to create check-in for.
     * @param squadCheckinCreateDto An object to create a squad check-in.
     * @return ResponseEntity with a newly created squadCheckinDTO object and a CREATED status code,
     * if the game or squad doesn't exist, will return a BAD_REQUEST status code.
     */
    @Operation(summary = "Create a new squad checkin for the given squad")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Checkin marker created successfully",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = SquadCheckinDto.class))}),
            @ApiResponse(responseCode = "404", description = "Squad is not found within given game"),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session")
    })
    @PostMapping("squad/{squadId}/check-in")
    // TODO: Only post marker if correct faction
    public ResponseEntity<SquadCheckinDto> createSquadCheckin(@PathVariable int gameId, @PathVariable int squadId, @RequestBody SquadCheckinCreateDto squadCheckinCreateDto) {
        HttpStatus status;
        if(!squadService.existsById(squadId, gameId)) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(status);
        }
        SquadCheckin squadCheckin = squadCheckinMapper.mapFromCreateDtoToEntity(squadCheckinCreateDto);

        squadCheckin.setGame(gameService.getGameById(gameId));
        squadCheckin.setSquad(squadService.getSquadById(gameId,squadId));
        squadCheckin.setSquadMember(squadMemberService.getSquadMemberById(squadCheckinCreateDto.getSquadMemberId()));

        squadCheckin = squadService.createSquadCheckin(gameId, squadId, squadCheckin);
        status = HttpStatus.CREATED;
        SquadCheckinDto squadCheckinDto = squadCheckinMapper.mapFromEntityToDto(squadCheckin);
        return new ResponseEntity<>(squadCheckinDto, status);
    }
}

// Squad Chat Mappings TODO needed?

    /*@GetMapping("squad/{squadId}/chat")
    public ResponseEntity<List<ChatDto>> getSquadChat(@PathVariable int gameId, @PathVariable int squadId) {
        List<Chat> chatList = squadService.getSquadChat(gameId, squadId);
        HttpStatus status;
        List<ChatDto> chatDtoList = chatMapper.mapFromEntityToDtoList(chatList);
        return new ResponseEntity<>(chatDtoList, status);
    }

    @PostMapping("squad/{squadId}/chat")
    public ResponseEntity<ChatDto> sendSquadChatMessage(@PathVariable int gameId, @PathVariable int squadId, @RequestBody ChatCreateDto chatCreateDto) {
        Chat chat = chatMapper.mapFromCreateDtoToEntity(chatCreateDto);
        chat = squadService.sendSquadChatMessage(gameId, squadId, chat);
        ChatDto chatDto = chatMapper.mapFromEntityToDto(chat);
        return new ResponseEntity<>(chatDto, status);
    }*/
