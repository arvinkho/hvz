package no.noroff.hvz.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirements;
import no.noroff.hvz.mappers.GameMapper;
import no.noroff.hvz.models.Game;
import no.noroff.hvz.models.dtos.GameCreateDto;
import no.noroff.hvz.models.dtos.GameDto;
import no.noroff.hvz.services.GameService;
import no.noroff.hvz.utils.GameState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/game")
public class GameController {
    @Autowired
    GameService gameService;
    @Autowired
    GameMapper gameMapper;

    // Game Mappings

    /**
     * Gets an existing game by the game id.
     * @param id game id of wanted game.
     * @return ResponseEntity with a gameDTO object and an OK status code,
     * if the game doesn't exist returns a BAD_REQUEST.
     */
    @Operation(summary = "Get specific game by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the game",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GameDto.class))}),
            @ApiResponse(responseCode = "404", description = "Game doesn't exist"),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session")
    })
    @GetMapping("{id}")
    public ResponseEntity<GameDto> getGameById(@PathVariable int id) {
        HttpStatus status = HttpStatus.OK;
        if(!gameService.existsById(id)) {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }
        Game game = gameService.getGameById(id);
        GameDto gameDto = gameMapper.mapFromEntityToDto(game);
        return new ResponseEntity<>(gameDto, status);
    }

    /**
     * Gets a list of all the games.
     * @return ResponseEntity with a gameDTO object list and an OK status code,
     * if no games exist returns NO_CONTENT.
     */
    @Operation(summary = "Get all games")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found all games",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GameDto.class))}),
            @ApiResponse(responseCode = "204", description = "No games exist")
    })
    // Remove security requirement for /game get endpoint
    @SecurityRequirements
    @GetMapping
    public ResponseEntity<List<GameDto>> getAllGames() {
        List<Game> gameList = gameService.getAllGames();
        HttpStatus status = HttpStatus.OK;
        if(gameList.size() == 0) {
            status = HttpStatus.NO_CONTENT;
            return new ResponseEntity<>(status);
        }
        List<GameDto> gameDtoList = gameMapper.mapFromEntityToDtoList(gameList);
        return new ResponseEntity<>(gameDtoList, status);
    }

    /**
     * Creates a game by receiving a gameCreateDTO object.
     * @param gameCreateDto Object with data to create game.
     * @return ResponseEntity with a newly created gameDTO object and a CREATED status code.
     */
    @Operation(summary = "Create a new game")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Game was created successfully",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GameDto.class))}),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session"),
            @ApiResponse(responseCode = "403",
                    description = "User doesn't have permission to create a game")
    })
    @PostMapping
    @PreAuthorize("hasAuthority('create:game')")
    public ResponseEntity<GameDto> createGame(@RequestBody GameCreateDto gameCreateDto) {
        Game game = gameMapper.mapFromCreateDtoToEntity(gameCreateDto);
        game = gameService.createGame(game);
        HttpStatus status = HttpStatus.CREATED;
        GameDto gameDto = gameMapper.mapFromEntityToDto(game);
        return new ResponseEntity<>(gameDto, status);
    }

    /**
     * Updates an existing game by the id, and a gameCreateDTO object.
     * @param id game id of wanted game.
     * @param gameCreateDto Object with data to update the game.
     * @return ResponseEntity with an updated gameDTO object and an OK status code,
     * if the game doesn't exist returns a BAD_REQUEST.
     */
    @Operation(summary = "Change the parameters of a game")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the game",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GameDto.class))}),
            @ApiResponse(responseCode = "404", description = "Game doesn't exist"),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session"),
            @ApiResponse(responseCode = "403",
                    description = "User doesn't have permission to update game")
    })
    @PutMapping("{id}")
    @PreAuthorize("hasAuthority('update:game')")
    public ResponseEntity<GameDto> updateGame(@PathVariable int id, @RequestBody GameCreateDto gameCreateDto) {
        HttpStatus status = HttpStatus.OK;
        if(!gameService.existsById(id)) {
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }
        Game game = gameMapper.mapFromCreateDtoToEntity(gameCreateDto);
        game = gameService.updateGame(id, game);
        GameDto gameDto = gameMapper.mapFromEntityToDto(game);
        return new ResponseEntity<>(gameDto, status);
    }

    /**
     * Deletes an existing game.
     * @param id game id of wanted game.
     * @return ResponseEntity with a NOT_FOUND status code or a NO_CONTENT status code if the game exists.
     */
    @Operation(summary = "Delete a game")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Game was deleted successfully"),
            @ApiResponse(responseCode = "404", description = "Game doesn't exist"),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session"),
            @ApiResponse(responseCode = "403",
                    description = "User doesn't have permission to delete game")
    })
    @DeleteMapping("{id}")
    @PreAuthorize("hasAuthority('delete:game')")
    public ResponseEntity<GameDto> deleteGame(@PathVariable int id) {
        HttpStatus status = HttpStatus.NOT_FOUND;
        if(gameService.existsById(id)) {
            gameService.deleteGame(id);
            status = HttpStatus.NO_CONTENT;
        }
        return new ResponseEntity<>(status);
    }

    // Game State Mappings

    /**
     * Updates the game state of a game.
     * @param id game id of wanted game.
     * @param gameState Enum of the state of the game, either registering, in progress or completed.
     * @return ResponseEntity with an updated gameDTO object and an OK status code,
     * if the game doesn't exist returns a BAD_REQUEST.
     */
    @Operation(summary = "Change the state of a game")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Game state changed successfully",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GameDto.class))}),
            @ApiResponse(responseCode = "400", description = "Game doesn't exist"),
            @ApiResponse(responseCode = "401",
                    description = "User isn't logged in to a valid session"),
            @ApiResponse(responseCode = "403",
                    description = "User doesn't have permission to update game")
    })
    @PatchMapping("{id}")
    @PreAuthorize("hasAuthority('update:game')")
    public ResponseEntity<GameDto> updateGameState(@PathVariable int id, @RequestBody GameState gameState) {
        HttpStatus status = HttpStatus.OK;
        if(!gameService.existsById(id)) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(status);
        }
        Game game = gameService.updateGameState(id, gameState);
        GameDto gameDto = gameMapper.mapFromEntityToDto(game);
        return new ResponseEntity<>(gameDto, status);
    }

    /**
     * Delete all games in the database. DEV ONLY
     * @return Request status
     */
    @Operation(description = "Deletes all games", hidden = true)
    @DeleteMapping
    @PreAuthorize("hasAuthority('delete:game')")
    public ResponseEntity<GameDto> deleteAllGames() {
        HttpStatus status = HttpStatus.NO_CONTENT;
        List<Game> games = gameService.getAllGames();
        for (Game game: games) {
            gameService.deleteGame(game);
        }
        return new ResponseEntity<>(status);
    }
}

// Chat Mappings TODO needed?

    /*@GetMapping("{id}/chat")
    public ResponseEntity<List<ChatDto>> getGameChatMessages(@PathVariable int id) {
        List<Chat> chatList = gameService.getGameChatMessages(id);
        HttpStatus status;
        List<ChatDto> chatDtoList = chatMapper.mapFromEntityToDtoList(chatList);
        return new ResponseEntity<>(chatDtoList, status);
    }

    @PostMapping("{id}/chat")
    public ResponseEntity<ChatDto> sendGameChatMessage(@PathVariable int id, @RequestBody ChatCreateDto chatCreateDto) {
        Chat chat = chatMapper.mapFromCreateDtoToEntity(chatCreateDto);
        chat = gameService.sendGameChatMessage(id, chat);
        HttpStatus status;
        ChatDto chatDto = chatMapper.mapFromEntityToDto(chat);
        return new ResponseEntity<>(chatDto, status);
    }*/
