package no.noroff.hvz.security;

import org.springframework.security.oauth2.core.OAuth2Error;
import org.springframework.security.oauth2.core.OAuth2ErrorCodes;
import org.springframework.security.oauth2.core.OAuth2TokenValidator;
import org.springframework.security.oauth2.core.OAuth2TokenValidatorResult;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.util.Assert;

import java.util.List;

// https://auth0.com/blog/spring-boot-authorization-tutorial-secure-an-api-java/

// Class to check if audience is as we expect
public class AudienceValidator implements OAuth2TokenValidator<Jwt> {

    private final String audience;

    AudienceValidator(String audience) {
        // audience can not be empty
        Assert.hasText(audience, "audience is null or empty");
        this.audience = audience;
    }

    /**
     * Validator for jwt audience
     * @param jwt Access token we want to validate
     * @return Validator for use in SecurityConfig
     */
    @Override
    public OAuth2TokenValidatorResult validate(Jwt jwt) {
        List<String> audiences = jwt.getAudience();
        // Check whether the audiences of the jwt contains the audience we want to use
        if (audiences.contains(audience)) {
            return OAuth2TokenValidatorResult.success();
        }

        OAuth2Error error = new OAuth2Error(OAuth2ErrorCodes.INVALID_TOKEN);

        return OAuth2TokenValidatorResult.failure(error);
    }
}
