package no.noroff.hvz.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.core.DelegatingOAuth2TokenValidator;
import org.springframework.security.oauth2.core.OAuth2TokenValidator;
import org.springframework.security.oauth2.jwt.*;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.List;

// https://auth0.com/blog/spring-boot-authorization-tutorial-secure-an-api-java/

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${auth0.audience}")
    private String audience;

    @Value("${spring.security.oauth2.resourceserver.jwt.issuer-uri}")
    private String issuer;

    /**
     * Configure our REST api security settings
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                // Require Https when using Heroku
                .requiresChannel()
                .requestMatchers(r -> r.getHeader("X-Forwarded-Proto") != null)
                .requiresSecure()
                .and()
                // Enable cross origin resources
                .cors()
                .configurationSource(corsConfigurationSource())
                .and()
                // Open endpoints
                .authorizeRequests()
                // Everyone has access to game list
                .mvcMatchers(HttpMethod.GET, "/api/v1/game", "/api/v1/docs/**").permitAll()
                // Chat endpoint is opened for all connections. Authentication happens in WebSocketHandler
                .mvcMatchers("/chat").permitAll()
                // TODO: Need to set session mngmnt to stateless?
                .anyRequest()
                .authenticated()
                .and()
                // Configure jwt authentication / authorization
                .oauth2ResourceServer()
                .jwt()
                .decoder(jwtDecoder())
                .jwtAuthenticationConverter(jwtAuthenticationConverter());
    }

    /**
     * Configure cross-origin resource sharing
     * @return Object used to config cors in the security config
     */
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration config = new CorsConfiguration();
        // Allow other points of origin to perform all http methods
        config.setAllowedMethods(
                List.of(
                        HttpMethod.GET.name(),
                        HttpMethod.PUT.name(),
                        HttpMethod.POST.name(),
                        HttpMethod.DELETE.name()
                )
        );

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", config.applyPermitDefaultValues());
        return source;
    }

    /**
     * Helper method for checking that jwt is valid, and has the correct audience and issuer
     * @return Jwt decoder for use in the http config
     */
    JwtDecoder jwtDecoder() {
        OAuth2TokenValidator<Jwt> hasAudience = new AudienceValidator(audience);
        OAuth2TokenValidator<Jwt> hasIssuer = JwtValidators.createDefaultWithIssuer(issuer);
        OAuth2TokenValidator<Jwt> validator = new DelegatingOAuth2TokenValidator<>(hasAudience, hasIssuer);

        NimbusJwtDecoder jwtDecoder = JwtDecoders.fromIssuerLocation(issuer);
        jwtDecoder.setJwtValidator(validator);
        return jwtDecoder;
    }

    /**
     * Helper method for changing authority claim and removing prefix. Used for auth0 compatibility
     * @return Authentication converter for http config
     */
    JwtAuthenticationConverter jwtAuthenticationConverter() {
        JwtGrantedAuthoritiesConverter converter = new JwtGrantedAuthoritiesConverter();
        // Config for auth0 (permissions rather than roles)
        converter.setAuthoritiesClaimName("permissions");
        converter.setAuthorityPrefix("");

        JwtAuthenticationConverter jwtConverter = new JwtAuthenticationConverter();
        jwtConverter.setJwtGrantedAuthoritiesConverter(converter);
        return jwtConverter;
    }
}
