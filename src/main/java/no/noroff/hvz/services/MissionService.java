package no.noroff.hvz.services;

import no.noroff.hvz.models.Mission;
import no.noroff.hvz.repositories.GameRepository;
import no.noroff.hvz.repositories.MissionRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MissionService {
    private final MissionRepository missionRepository;
    private final GameRepository gameRepository;

    public MissionService(MissionRepository missionRepository, GameRepository gameRepository) {
        this.missionRepository = missionRepository;
        this.gameRepository = gameRepository;
    }

    /**
     * Helper method for checking if the specified mission exists within the specified game
     * @param missionId Id of the mission we want to see if exists
     * @param gameId Id of the game the mission should be in
     * @return True if mission exists in specified game, false if not
     */
    public boolean existsById(int missionId, int gameId) {
        return gameRepository.existsById(gameId) &&
                missionRepository.existsByMissionIdAndGame(missionId, gameRepository.getById(gameId));
    }

    /**
     * Get all missions in a game
     * @param gameId Id of the game we want to get missions from
     * @return List of all missions in the game
     */
    public List<Mission> getAllMissions(int gameId) {
        return missionRepository.getAllByGame(gameRepository.getById(gameId));
    }

    /**
     * Get a mission from a game by their ids
     * @param gameId Id of game mission is in
     * @param missionId Id of the mission
     * @return Mission with the correct mission and game id
     */
    public Mission getMissionById(int gameId, int missionId) {
        return missionRepository.getByMissionIdAndGame(missionId, gameRepository.getById(gameId));
    }

    /**
     * Create a new mission in the specified game
     * @param gameId Id of the game we want to add mission to
     * @param mission The body of the new mission
     * @return The newly created mission entity
     */
    public Mission createMission(int gameId, Mission mission) {
        mission.setGame(gameRepository.getById(gameId));
        return missionRepository.save(mission);
    }

    /**
     * Change the body of a mission with the appropriate game and mission id
     * @param gameId Id of game the mission is in
     * @param missionId Id of the mission itself
     * @param mission The new body of the mission
     * @return The updated mission entity
     */
    public Mission updateMission(int gameId, int missionId, Mission mission) {
        mission.setMissionId(missionId);
        mission.setGame(gameRepository.getById(gameId));
        return missionRepository.save(mission);
    }

    /**
     * Delete the mission with the corresponding mission and game id.
     * @param gameId Id of the game the mission is in
     * @param missionId Id of the mission
     */
    public void deleteMission(int gameId, int missionId) {
        missionRepository.deleteByMissionIdAndGame(missionId, gameRepository.getById(gameId));
    }
}
