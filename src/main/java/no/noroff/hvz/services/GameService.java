package no.noroff.hvz.services;

import no.noroff.hvz.models.Game;
import no.noroff.hvz.repositories.GameRepository;
import no.noroff.hvz.utils.GameState;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GameService {
    private final GameRepository gameRepository;

    public GameService(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    /**
     * Check if a game exists with the given id
     * @param id Id of game we want to check existence of
     * @return True if a game exists with given id, false if not
     */
    public boolean existsById(int id) {
        return gameRepository.existsById(id);
    }

    /**
     * Get all games currently in the database
     * @return List of all games in database
     */
    public List<Game> getAllGames() {
        return gameRepository.findAll();
    }

    /**
     * Get a specific game based on id
     * @param id Id of the game we want to get
     * @return The game entity with given id
     */
    public Game getGameById(int id) {
        return gameRepository.getById(id);
    }

    /**
     * Save a new game to the database
     * @param game Game to save to database
     * @return The saved game
     */
    public Game createGame(Game game) {
        return gameRepository.save(game);
    }

    /**
     * Update an already existing game.
     * @param id Id of game to update.
     * @param game Body of game to update.
     * @return The updated game.
     */
    public Game updateGame(int id, Game game) {
        // Make sure the id in body is correct.
        game.setGameId(id);
        return gameRepository.save(game);
    }

    /**
     * Delete a game based on id
     * @param id Id of game we want to delete
     */
    public void deleteGame(int id) {
        gameRepository.deleteById(id);
    }

    /**
     * Delete a game by entity
     * @param game Game entity we want to delete from database
     */
    public void deleteGame(Game game) {
        gameRepository.delete(game);
    }

    /**
     * Change the state of a game
     * @param id Id of the game we want to change the state of
     * @param gameState The state we want to change to
     * @return The updated game
     */
    public Game updateGameState(int id, GameState gameState) {
        Game game = gameRepository.getById(id);
        game.setGameState(gameState);
        return gameRepository.save(game);
    }
}
