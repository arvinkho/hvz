package no.noroff.hvz.services;

import no.noroff.hvz.models.*;
import no.noroff.hvz.repositories.*;
import no.noroff.hvz.utils.SquadRank;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SquadService {
    private final SquadRepository squadRepository;
    private final SquadMemberRepository squadMemberRepository;
    private final GameRepository gameRepository;
    private final SquadCheckinRepository squadCheckinRepository;
    private final UserRepository userRepository;

    public SquadService(SquadRepository squadRepository, SquadMemberRepository squadMemberRepository, GameRepository gameRepository, SquadCheckinRepository squadCheckinRepository, UserRepository userRepository) {
        this.squadRepository = squadRepository;
        this.squadMemberRepository = squadMemberRepository;
        this.gameRepository = gameRepository;
        this.squadCheckinRepository = squadCheckinRepository;
        this.userRepository = userRepository;
    }

    /**
     * Find a squad with a given id inside the given game
     * @param squadId Id of squad we want to check existence of
     * @param gameId Id of game squad should exist in
     * @return True if squad exists in game, false if not
     */
    public boolean existsById(int squadId, int gameId) {
        return gameRepository.existsById(gameId) &&
                squadRepository.existsBySquadIdAndGame(squadId, gameRepository.getById(gameId));
    }

    /**
     * Check if a player is already part of a squad. Also checks whether player is in the given game
     * @param player Player we want to check
     * @param game Game player is in
     * @return True if player is in a squad, false if not
     */
    public boolean playerAlreadyInSquad(Player player, Game game) {
        return squadMemberRepository.existsByPlayerAndGame(player, game);
    }

    /**
     * Get all squads in a game
     * @param gameId Game we want to get squads from
     * @return List of squads in game
     */
    public List<Squad> getAllSquads(int gameId) {
        return squadRepository.getAllByGame(gameRepository.getById(gameId));
    }

    /**
     * Get a squad with specific id from a given game
     * @param gameId Id of the game we want to get squad from
     * @param squadId Id of the squad we want to find
     * @return The squad with the corresponding game and squad ids
     */
    public Squad getSquadById(int gameId, int squadId) {
        return squadRepository.getBySquadIdAndGame(squadId, gameRepository.getById(gameId));
    }

    /**
     * Get the squad of a specific squad member
     * @param squadMemberId Id of the squad member we want to get the squad of
     * @return The squad the member is part of
     */
    public Squad getSquadBySquadMemberId(int squadMemberId) {
        SquadMember squadMember = squadMemberRepository.getById(squadMemberId);
        return squadMember.getSquad();
    }

    /**
     * Create a new squad in a game
     * @param game Reference to the game we want to create the squad in
     * @param squad Body of the squad we want to create
     * @param player The player that made the squad
     * @return The created squad entity
     */
    public Squad createSquad(Game game, Squad squad, Player player) {
        squad.setGame(game);
        SquadMember member = new SquadMember();
        member.setPlayer(player);
        member.setRank(SquadRank.LEADER);
        squadMemberRepository.save(member);
        member.setSquad(squad);
        member.setGame(game);
        squad.setSquadMemberList(List.of(member));
        return squadRepository.save(squad);
    }

    /**
     * Create a new member within a squad
     * @param gameId Id of the game where the squad and player are located
     * @param squadId Id of the squad we want to add player to
     * @param squadMember Body of the squad member we want to create
     * @return The created squad member
     */
    public SquadMember createSquadMember(int gameId, int squadId, SquadMember squadMember) {
        Squad squad = squadRepository.getBySquadIdAndGame(squadId, gameRepository.getById(gameId));
        squadMember.setSquad(squad);
        // TODO: might have to use setSquadMemberList ?
        squad.getSquadMemberList().add(squadMember);
        return squadMemberRepository.save(squadMember);
    }

    /**
     * Change the body of a squad
     * @param gameId Id of the game the squad is in
     * @param squadId Id of the squad we want to change
     * @param squad The updated body of the squad
     * @return The updated squad entity
     */
    public Squad updateSquad(int gameId, int squadId, Squad squad) {
        squad.setSquadId(squadId);
        squad.setGame(gameRepository.getById(gameId));
        return squadRepository.save(squad);
    }

    /**
     * Get all checkin markers that belong to a given squad
     * @param gameId Id of the game the squad is in
     * @param squad Id of the squad we want to get markers of
     * @param isAdmin If the player is admin we always want to return all checkin markers
     * @param player The player requesting the markers
     * @return List of squad checkin markers connected to the given squad that the player is allowed to see
     */
    public List<SquadCheckin> getSquadCheckinMarkers(int gameId, Squad squad, Player player, boolean isAdmin) {
        List<SquadCheckin> allCheckins = squadCheckinRepository.getAllByGameAndSquad(
                gameRepository.getById(gameId),
                squad);
        // If the player is admin, return all checkin markers.
        if (isAdmin)
            return allCheckins;

        // TODO: Might be unnecessary. We already check if the player is allowed to see the markers in controller.
        return allCheckins.stream().filter(checkin ->
                checkin.getSquad().isHuman() == player.isHuman())
                .collect(Collectors.toList());
    }

    /**
     * Create a new checkin marker for the given squad
     * @param gameId Id of the game the squad should be located in
     * @param squadId Id of the squad the marker should be added to
     * @param squadCheckin Body of the new marker
     * @return The new checkin marker entity
     */
    public SquadCheckin createSquadCheckin(int gameId, int squadId, SquadCheckin squadCheckin) {
        squadCheckin.setSquad(squadRepository.getBySquadIdAndGame(squadId, gameRepository.getById(gameId)));
        return squadCheckinRepository.save(squadCheckin);
    }

    /**
     * Remove a squad from game and database
     * @param gameId Id of the game the squad is in
     * @param squadId Id of the squad to delete
     */
    public void deleteSquad(int gameId, int squadId) {
        squadRepository.deleteBySquadIdAndGame(squadId, gameRepository.getById(gameId));
    }

    /**
     * Delete a squad member permanently
     * @param squadMemberId Id of the squad member to delete
     * @param squadId Id of the squad the member should be in
     */
    public void deleteSquadMember(int squadMemberId, int squadId) {
        Squad squad = squadRepository.getById(squadId);
        List<SquadMember> squadMembers = squad.getSquadMemberList();
        SquadMember squadMember = squadMemberRepository.getById(squadMemberId);
        squadMembers.remove(squadMember);
        squadMemberRepository.delete(squadMember);
        if (squadMembers.size() < 1) {
            this.deleteSquad(squad.getGame().getGameId(), squadId);
        } else {
            squad.setSquadMemberList(squadMembers);
            squadRepository.save(squad);
        }

    }
}
