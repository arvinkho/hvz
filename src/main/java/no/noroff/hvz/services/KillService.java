package no.noroff.hvz.services;

import no.noroff.hvz.models.Game;
import no.noroff.hvz.models.Kill;
import no.noroff.hvz.models.Player;
import no.noroff.hvz.repositories.GameRepository;
import no.noroff.hvz.repositories.KillRepository;
import no.noroff.hvz.repositories.PlayerRepository;
import no.noroff.hvz.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KillService {
    private final KillRepository killRepository;
    private final GameRepository gameRepository;
    private final PlayerRepository playerRepository;
    private final UserRepository userRepository;

    public KillService(KillRepository killRepository, GameRepository gameRepository,
                       PlayerRepository playerRepository, UserRepository userRepository) {
        this.killRepository = killRepository;
        this.gameRepository = gameRepository;
        this.playerRepository = playerRepository;
        this.userRepository = userRepository;
    }

    /**
     * Helper method for checking if a bite code is valid. Used in controller.
     * @param biteCode String used to check if there exists a player with the corresponding bite code.
     * @param gameId Id of the game we want to check bite codes in.
     * @return True if a human player with the right code exists in the game, false if not.
     */
    public boolean validBiteCode(String biteCode, int gameId) {
        Game game = gameRepository.getById(gameId);
        return playerRepository.existsByBiteCodeAndGame(biteCode, game) &&
                playerRepository.getByBiteCodeAndGame(biteCode, game).isHuman();
    }

    /**
     * Checks whether a game exists, then if a kill exists within a specified game
     * @param killId Id of the kill we want to check if exists
     * @param gameId Id of the game we want to check if kill exists in
     * @return True if kill exists in game, false if not.
     */
    public boolean existsById(int killId, int gameId) {
        return gameRepository.existsById(gameId) &&
                killRepository.existsByKillIdAndGame(killId, gameRepository.getById(gameId));
    }

    public boolean existsByBiteCodeAndGameId(String biteCode, int gameId) {
        return killRepository.existsByVictim(
                playerRepository.getByBiteCodeAndGame(biteCode, gameRepository.getById(gameId)));
    }

    /**
     * Gets all kills in a game.
     * @param gameId Id of the game to get all kills in.
     * @return All kills in the given game.
     */
    public List<Kill> getAllKills(int gameId) {
        return killRepository.getAllByGame(gameRepository.getById(gameId));
    }

    /**
     * Get a specific kill in a game. We could technically just use the killId, as it is individual per
     * kill, not just per kill, per game, but this makes it so that we can't accidentally return
     * a kill from a different game than requested.
     * @param gameId Id of the game the kill is in.
     * @param killId Id of the kill itself.
     * @return The specified kill.
     */
    public Kill getKillById(int gameId, int killId) {
        return killRepository.getByKillIdAndGame(killId, gameRepository.getById(gameId));
    }

    /**
     * Create a new kill in a game, only if the bite code is valid (checked in controller)
     * @param gameId Id of the game to add a kill to.
     * @param kill Body of the kill.
     * @param bite_code Bite code of the victim.
     * @param killerUserId User id of the killer
     * @return The newly created kill
     */
    public Kill createKill(int gameId, Kill kill, String bite_code, String killerUserId) {
        kill.setTimeOfDeath(System.currentTimeMillis());
        Game game = gameRepository.getById(gameId);
        Player killer = userRepository.getById(killerUserId).getPlayer();
        Player victim = playerRepository.getByBiteCodeAndGame(bite_code, game);
        killer.getKills().add(kill);
        victim.setKilledBy(kill);
        kill.setGame(game);
        kill.setKiller(killer);
        kill.setVictim(victim);
        victim.setHuman(false);
        return killRepository.save(kill);
    }

    /**
     * Change the body of specific kill to the body provided in the put request.
     * @param gameId Id of game the kill is in
     * @param killId Id of the kill to update
     * @param kill Body of the updated kill
     * @return Updated kill entity
     */
    public Kill updateKill(int gameId, int killId, Kill kill) {
        // Make sure the id in the body is correct.
        kill.setKillId(killId);
        kill.setGame(gameRepository.getById(gameId));
        return killRepository.save(kill);
    }

    /**
     * Deletes a kill by specified game and kill Ids
     * @param gameId Id for the game of the kill we should delete
     * @param killId Id of the kill we should delete
     */
    public void deleteKill(int gameId, int killId) {
        killRepository.deleteByKillIdAndGame(killId, gameRepository.getById(gameId));
    }
}
