package no.noroff.hvz.services;

import no.noroff.hvz.models.Users;
import no.noroff.hvz.repositories.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    /**
     * Get user by id.
     * @param userId the specific id of the user to get.
     * @return User that matches the id.
     */
    public Users getUserById(String userId){
            return userRepository.getById(userId);
    }

    /**
     * Returns true or false whether the user exists or not.
     * @param userId id of user we want to check if exists.
     * @return True or false.
     */
    public boolean userExistsById(String userId){
        return userRepository.existsById(userId);
    }

    /**
     * Save a user to the database
     * @param user User we want to save
     * @return The saved user
     */
    public Users saveUser(Users user) {
        return userRepository.save(user);
    }

    /**
     * Check if a user exists already
     * @param userId Id of the user we want to check
     * @return True if user exists, false if not
     */
    public boolean existsById(String userId) {
        return userRepository.existsById(userId);
    }
}
