package no.noroff.hvz.services;

import no.noroff.hvz.models.Player;
import no.noroff.hvz.models.Squad;
import no.noroff.hvz.models.SquadMember;
import no.noroff.hvz.models.Users;
import no.noroff.hvz.repositories.GameRepository;
import no.noroff.hvz.repositories.PlayerRepository;
import no.noroff.hvz.repositories.SquadRepository;
import no.noroff.hvz.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PlayerService {
    private final PlayerRepository playerRepository;

    private final GameRepository gameRepository;

    private final UserRepository userRepository;

    private final SquadRepository squadRepository;

    public PlayerService(PlayerRepository playerRepository, GameRepository gameRepository, UserRepository userRepository,
                         SquadRepository squadRepository) {
        this.playerRepository = playerRepository;
        this.gameRepository = gameRepository;
        this.userRepository = userRepository;
        this.squadRepository = squadRepository;
    }

    /**
     * Checks whether a player exists in a particular game or not.
     * @param playerId Id of player.
     * @param gameId Id of the game.
     * @return true or false.
     */
    public boolean existsByPlayerAndGameId(int playerId, int gameId){
        return playerRepository.existsPlayerByIdAndGame(playerId, gameRepository.getById(gameId));
    }

    /**
     * Check if a player exists with the given id
     * @param playerId Id of the player we want to check
     * @return True if player exists, false if not
     */
    public boolean existsById(int playerId) {
        return playerRepository.existsById(playerId);
    }

    /**
     * Check if a user has already registered in a game
     * @param gameId Id of the game the user is attempting to register for
     * @param userId Id of the user
     * @return True or false
     */
    public boolean existsByGameIdAndUserId(int gameId, String userId) {
        return userRepository.existsById(userId) &&
                playerRepository.existsByGameAndUser(gameRepository.getById(gameId),
                        userRepository.getById(userId));
    }

    /**
     * Get all players in database
     * @return List of all current players
     */
    public List<Player> getAllPlayers() {
        return playerRepository.findAll();
    }

    /**
     * Gets all players in a particular game
     * @param gameId Id of the game we want to get all players from.
     * @return The list of players.
     */
    public List<Player> getAllPlayersInGame(int gameId){
        return playerRepository.getAllByGame(gameRepository.getById(gameId));
    }

    /**
     * Get all players in a given squad
     * @param squadId Id of the squad we want to get the players of
     * @return List of all players in the squad
     */
    public List<Player> getAllPlayersInSquad(int squadId){
        Squad squad = this.squadRepository.getById(squadId);
        List<SquadMember> squadMembers = squad.getSquadMemberList();
        List<Player> players = squadMembers.stream().map(SquadMember::getPlayer).collect(Collectors.toList());
        return players;
    }


    /**
     * Gets player by specific id.
     * @param id the id of player to get.
     * @return The player with mathing id.
     */
    public Player getPlayerById(int id){
        return playerRepository.getById(id);
    }

    /**
     * Register a new player in a game
     * @param gameId Id of the game the player should be registered in
     * @param player The player to register
     * @param userId Id of the user the player be created for
     * @return Registered player
     */
    public Player registerPlayer(int gameId, Player player, String userId){
        player.setGame(gameRepository.getById(gameId));
        Users user;
        // Check if the user that sent the get request is already saved in the database
        if (userRepository.existsById(userId)) {
            user = userRepository.getById(userId);
            // TODO: Move the deletion of player to gameService, when the gameState changes to finished.
            // Check if the user already played in a game, remove the previous player if they have.
            if (playerRepository.existsByUser(user))
                playerRepository.delete(user.getPlayer());
        }
        else {
            // Save them in database if not.
            user = new Users();
            user.setUserId(userId);
            userRepository.save(user);
        }
        // Link the player and user
        user.setPlayer(player);
        player.setUser(user);
        return playerRepository.save(player);
    }

    /**
     * Updates a players body with the submitted data.
     * @param player player with the data to update with.
     * @param playerId id of player to update.
     * @return updated Player.
     */
    public Player updatePlayerInGame(Player player, int playerId){
        player.setId(playerId);
        return playerRepository.save(player);
    }

    /**
     * Deletes player from DB.
     * @param playerId id of player to delete.
     */
    public void deletePlayer(int playerId){
        playerRepository.deleteById(playerId);
    }
}
