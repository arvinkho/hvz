package no.noroff.hvz.services;

import no.noroff.hvz.models.SquadMember;
import no.noroff.hvz.repositories.SquadMemberRepository;
import org.springframework.stereotype.Service;

@Service
public class SquadMemberService {
    private final SquadMemberRepository squadMemberRepository;

    public SquadMemberService(SquadMemberRepository squadMemberRepository) {
        this.squadMemberRepository = squadMemberRepository;
    }

    /**
     * Get a squad member by their id
     * @param id Id of squad member we want to get
     * @return The relevant squad member entity
     */
    public SquadMember getSquadMemberById(int id){
        return squadMemberRepository.getById(id);
    }

    /**
     * Check if a squad member exists with the given id
     * @param id Id of squad member to check
     * @return True if member exists, false if not
     */
    public boolean existsById(int id) {
        return squadMemberRepository.existsById(id);
    }

}
