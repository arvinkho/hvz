package no.noroff.hvz.services;

import no.noroff.hvz.models.SquadCheckin;
import no.noroff.hvz.repositories.SquadCheckinRepository;
import org.springframework.stereotype.Service;

@Service
public class SquadCheckinService {
    private final SquadCheckinRepository squadCheckinRepository;

    public SquadCheckinService(SquadCheckinRepository squadCheckinRepository) {
        this.squadCheckinRepository = squadCheckinRepository;
    }

    public SquadCheckin getSquadCheckinById(int id){
        return squadCheckinRepository.getById(id);
    }
}
