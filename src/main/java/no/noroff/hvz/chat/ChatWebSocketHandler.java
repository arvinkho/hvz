package no.noroff.hvz.chat;

import no.noroff.hvz.services.UserService;
import no.noroff.hvz.utils.JsonWebTokenUtil;
import no.noroff.hvz.utils.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.PingMessage;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ChatWebSocketHandler extends TextWebSocketHandler {

    private final String webSocketAuthHeader = "Sec-WebSocket-Protocol";
    private final String invalidSessionMessage = "Invalid user info";
    private final List<WebSocketSession> webSocketSessions = new ArrayList<>();

    @Autowired
    JsonWebTokenUtil tokenUtil;
    @Autowired
    UserService userService;

    public ChatWebSocketHandler() {
        // Make a thread to ping all sessions periodically.
        new Thread(new PingTask()).start();
    }

    // The task the pinging thread executes
    private class PingTask implements Runnable {
        @Override
        public void run() {
            // We want to always ping clients, the only time we stop is when the server closes
            while (true){
                // Ping all sessions
                for (WebSocketSession session: webSocketSessions) {
                    try {
                        session.sendMessage(new PingMessage());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                // Then wait for 30 sec
                try {
                    Thread.sleep(30000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        boolean validSession;

        // Authentication of session
        try {
            validSession = validSession(session);
        } catch (NullPointerException e) {
            // NullPointerException is only thrown if the user didn't add a token, meaning they're not logged in
            validSession = false;
        }

        // If the audience or domain is invalid, the user shouldn't be able to connect
        if (!validSession) {
            session.close(new CloseStatus(CloseStatus.PROTOCOL_ERROR.getCode(), invalidSessionMessage));
            return;
        }
        webSocketSessions.add(session);
        // TODO ADD BROADCAST OF USER JOINED
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        for (WebSocketSession webSocketSession : webSocketSessions) {
            webSocketSession.sendMessage(message);
        }
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        webSocketSessions.remove(session);
        // TODO ADD BROADCAST OF USER LEFT
    }

    /**
     * Helper method for checking if a valid user is attempting to connect
     * @param session The session the user is connecting from.
     * @return True if valid user, false if not.
     * @throws NullPointerException If the connecting user didn't send a jwt for authentication.
     */
    private boolean validSession(WebSocketSession session) throws NullPointerException {
        UserInfo userInfo = tokenUtil.getUserInfo(
                session.getHandshakeHeaders()
                        .get(webSocketAuthHeader)
                        .get(0));
        return userInfo.validAudience() && userInfo.validDomain() && userService.existsById(userInfo.userId());
    }
}
