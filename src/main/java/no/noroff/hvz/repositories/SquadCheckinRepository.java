package no.noroff.hvz.repositories;

import no.noroff.hvz.models.Game;
import no.noroff.hvz.models.Squad;
import no.noroff.hvz.models.SquadCheckin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SquadCheckinRepository extends JpaRepository<SquadCheckin, Integer> {
    /**
     * Get all squad checkins from a specific squad in a specific game
     * @param game Game entity the squad checkins belong to
     * @param squad Squad entity the checkins belong to
     * @return List of squad checking
     */
    List<SquadCheckin> getAllByGameAndSquad(Game game, Squad squad);
}
