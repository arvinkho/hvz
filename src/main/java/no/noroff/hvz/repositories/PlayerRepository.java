package no.noroff.hvz.repositories;

import no.noroff.hvz.models.Game;
import no.noroff.hvz.models.Player;
import no.noroff.hvz.models.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface PlayerRepository extends JpaRepository<Player, Integer> {
    /**
     * Get all players in a game
     * @param game The game to get players from
     * @return List of players in game
     */
    List<Player> getAllByGame(Game game);

    /**
     * Get a specific player in a game
     * @param playerId Id of the player to get from game
     * @param game Game entity player should be associated with
     * @return Player with corresponding id and game reference
     */
    Player getPlayerByIdAndGame(int playerId, Game game);

    /**
     * Remove player from game and delete from database
     * @param playerId Id of player to delete
     * @param game Game entity the player should refer to
     */
    @Transactional
    void deletePlayerByIdAndGame(int playerId, Game game);

    /**
     * Check if a player exists within the specified game
     * @param playerId Id of player we want to find
     * @param game Game player should be in
     * @return True if player exists within game, false if not
     */
    boolean existsPlayerByIdAndGame(int playerId, Game game);

    /**
     * Check if a player with the given bite code exists within the given game
     * @param biteCode Code to check existence of
     * @param game Game we want to see if bite code exists in
     * @return True if a player with the given bite code exists in the game, false if not
     */
    boolean existsByBiteCodeAndGame(String biteCode, Game game);

    /**
     * Check if a player is already registered in a game
     * @param game Game the player might be registered in
     * @param user The user that is registering
     * @return True or false
     */
    boolean existsByGameAndUser(Game game, Users user);

    /**
     * Get a specific player in the game that has the given bite code.
     * @param biteCode Bite code of the player we want to find
     * @param game Game entity the player should refer to
     * @return Player within the specified game with the correct bite code.
     */
    Player getByBiteCodeAndGame(String biteCode, Game game);

    /**
     * Check if the user is registered for a game
     * @param user The user we want to check registration status of
     * @return True if user is already player in a game, false if not
     */
    boolean existsByUser(Users user);
}


