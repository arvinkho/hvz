package no.noroff.hvz.repositories;

import no.noroff.hvz.models.Game;
import no.noroff.hvz.models.Player;
import no.noroff.hvz.models.SquadMember;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SquadMemberRepository extends JpaRepository<SquadMember, Integer> {
    /**
     * Check if a player is part of a squad within a given game
     * @param player The player we want to check squad status of
     * @param game The game we want to check if player is in
     * @return True if player is part of a squad within the game, false if not
     */
    boolean existsByPlayerAndGame(Player player, Game game);
}
