package no.noroff.hvz.repositories;

import no.noroff.hvz.models.Game;
import no.noroff.hvz.models.Mission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface MissionRepository extends JpaRepository<Mission, Integer> {
    /**
     * Check if a specific mission belongs to a specific game
     * @param missionId Id of the mission to check
     * @param game Game entity the mission should refer to
     * @return True if mission exists in game, false if not.
     */
    boolean existsByMissionIdAndGame(int missionId, Game game);

    /**
     * Get all missions in a game
     * @param game The game to get missions for
     * @return List of all missions in a game
     */
    List<Mission> getAllByGame(Game game);

    /**
     * Get a specific mission from a game
     * @param missionId Id of the mission we want to get
     * @param game Game the mission should exist in
     * @return The mission from the specified game
     */
    Mission getByMissionIdAndGame(int missionId, Game game);

    /**
     * Remove a mission from a game and delete it from the database
     * @param missionId Id of the mission we want to remove
     * @param game The game entity the mission should refer to
     */
    @Transactional
    void deleteByMissionIdAndGame(int missionId, Game game);
}