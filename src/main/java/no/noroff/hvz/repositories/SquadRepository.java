package no.noroff.hvz.repositories;

import no.noroff.hvz.models.Game;
import no.noroff.hvz.models.Squad;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface SquadRepository extends JpaRepository<Squad, Integer> {
    /**
     * Check if the squad exists within a given game
     * @param squadId Id of squad we want to check existence of
     * @param byId The game we want to check if squad is in
     * @return True if squad is in the given game, false if not
     */
    boolean existsBySquadIdAndGame(int squadId, Game byId);

    /**
     * Get all squads in a game
     * @param game Game we want to get the squads in
     * @return List of all squads in the given game
     */
    List<Squad> getAllByGame(Game game);

    /**
     * Get a squad in a given game based on the squad id
     * @param squadId Id of the squad we want to find
     * @param game Game the squad should be in
     * @return The squad with the correct id
     */
    Squad getBySquadIdAndGame(int squadId, Game game);

    /**
     * Delete a squad based on game it's in and squad id
     * @param squadId Id of squad to delete
     * @param game Game squad should be in
     */
    @Transactional
    void deleteBySquadIdAndGame(int squadId, Game game);
}
