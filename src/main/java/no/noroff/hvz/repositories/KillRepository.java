package no.noroff.hvz.repositories;

import no.noroff.hvz.models.Game;
import no.noroff.hvz.models.Kill;
import no.noroff.hvz.models.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface KillRepository extends JpaRepository<Kill, Integer> {
    /**
     * Checks if the specified kill exists. Only returns true if there exists a kill with correct id
     * and with a reference to the correct game
     * @param killId Id of the kill we want to see if exists.
     * @param game Reference to the game the kill should be in.
     * @return True if the kill exists within the specified game, false if not.
     */
    boolean existsByKillIdAndGame(Integer killId, Game game);

    boolean existsByVictim(Player victim);

    /**
     * Get all kills belonging to specified game
     * @param game Game entity the kills should be referencing
     * @return A list of kills in the specified game.
     */
    List<Kill> getAllByGame(Game game);

    /**
     * Get a specific kill within a game
     * @param killId Id of the kill we want to get
     * @param game Game entity the kill should be referencing
     * @return The kill that matches both killId and game reference.
     */
    Kill getByKillIdAndGame(int killId, Game game);

    /**
     * Delete a specific kill within a game.
     * @param killId Id of the kill to delete
     * @param game Reference to the game entity the kill should be linked to
     */
    @Transactional
    void deleteByKillIdAndGame(int killId, Game game);
}
