package no.noroff.hvz.models;


import javax.persistence.*;
import java.util.List;

@Entity
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private boolean isHuman;
    private boolean isPatientZero;
    private String biteCode;
    private String name;

    //Relations:

    @OneToOne
    @JoinColumn(name = "user_id")
    private Users user;

    @ManyToOne
    @JoinColumn(name = "game_id")
    private Game game;

    @OneToMany(mappedBy = "killer", cascade = CascadeType.REMOVE)
    private List<Kill> kills;

    @OneToOne(mappedBy = "victim", cascade = CascadeType.REMOVE)
    private Kill killedBy;

    @OneToOne(mappedBy = "player", cascade = CascadeType.REMOVE)
    private SquadMember squadMember;

    public Player(Integer id, boolean isHuman, boolean isPatientZero,
                  String biteCode, Users user, Game game, List<Kill> kills, Kill killedBy, SquadMember squadMember) {
        this.id = id;
        this.isHuman = isHuman;
        this.isPatientZero = isPatientZero;
        this.biteCode = biteCode;
        this.user = user;
        this.game = game;
        this.kills = kills;
        this.killedBy = killedBy;
        this.squadMember = squadMember;
        this.name = this.user.getName();

    }

    public Player() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isHuman() {
        return isHuman;
    }

    public void setHuman(boolean human) {
        isHuman = human;
    }

    public boolean isIsPatientZero() {
        return isPatientZero;
    }

    public void setIsPatientZero(boolean is_patient_zero) {
        this.isPatientZero = is_patient_zero;
    }

    public String getBiteCode() {
        return biteCode;
    }

    public void setBiteCode(String bite_code) {
        this.biteCode = bite_code;
    }


    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public List<Kill> getKills() {
        return kills;
    }

    public void setKills(List<Kill> kills) {
        this.kills = kills;
    }

    public Kill getKilledBy() {
        return killedBy;
    }

    public void setKilledBy(Kill killedBy) {
        this.killedBy = killedBy;
    }

    public SquadMember getSquadMember() {
        return squadMember;
    }

    public void setSquadMember(SquadMember squadMember) {
        this.squadMember = squadMember;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
