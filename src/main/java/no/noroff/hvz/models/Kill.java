package no.noroff.hvz.models;

import javax.persistence.*;

@Entity
public class Kill {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer killId;

    private Long timeOfDeath;
    private String story;
    private Double lat;
    private Double lng;

    //Relations

    @ManyToOne
    @JoinColumn(name = "game_id")
    private Game game;


    @ManyToOne
    @JoinColumn(name = "killer_id")
    private Player killer;

    @OneToOne
    @JoinColumn(name = "victim_id")
    private Player victim;

    public Kill(Integer killId, Long timeOfDeath, String story, Double lat, Double lng,
                Game game, Player killer, Player victim) {
        this.killId = killId;
        this.timeOfDeath = timeOfDeath;
        this.story = story;
        this.lat = lat;
        this.lng = lng;
        this.game = game;
        this.killer = killer;
        this.victim = victim;
    }

    public Kill() {
    }

    public Integer getKillId() {
        return killId;
    }

    public void setKillId(Integer kill_id) {
        this.killId = kill_id;
    }

    public Long getTimeOfDeath() {
        return timeOfDeath;
    }

    public void setTimeOfDeath(Long time_of_death) {
        this.timeOfDeath = time_of_death;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Player getKiller() {
        return killer;
    }

    public void setKiller(Player killer) {
        this.killer = killer;
    }

    public Player getVictim() {
        return victim;
    }

    public void setVictim(Player victim) {
        this.victim = victim;
    }
}
