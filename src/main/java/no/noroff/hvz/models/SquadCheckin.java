package no.noroff.hvz.models;


import javax.persistence.*;

@Entity
public class SquadCheckin {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer squadCheckinId;
    private Long startTime;
    private Long endTime;
    private double lat;
    private double lng;

    //Relations
    @ManyToOne
    @JoinColumn(name = "game_id")
    private Game game;

    @ManyToOne
    @JoinColumn(name = "squad_id")
    private Squad squad;

    @ManyToOne
    @JoinColumn(name = "squadMember_id")
    private SquadMember squadMember;

    public SquadCheckin(Integer squadCheckinId, Long startTime, Long endTime,
                        double lat, double lng, Game game, Squad squad, SquadMember squadMember) {
        this.squadCheckinId = squadCheckinId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.lat = lat;
        this.lng = lng;
        this.game = game;
        this.squad = squad;
        this.squadMember = squadMember;
    }

    public SquadCheckin() {
    }

    public Integer getSquadCheckinId() {
        return squadCheckinId;
    }

    public void setSquadCheckinId(Integer squad_checkin_id) {
        this.squadCheckinId = squad_checkin_id;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long start_time) {
        this.startTime = start_time;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long end_time) {
        this.endTime = end_time;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Squad getSquad() {
        return squad;
    }

    public void setSquad(Squad squad) {
        this.squad = squad;
    }

    public SquadMember getSquadMember() {
        return squadMember;
    }

    public void setSquadMember(SquadMember squadMember) {
        this.squadMember = squadMember;
    }
}
