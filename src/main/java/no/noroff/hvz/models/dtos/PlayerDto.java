package no.noroff.hvz.models.dtos;

import no.noroff.hvz.models.Game;
import no.noroff.hvz.models.Player;
import no.noroff.hvz.models.SquadMember;
import no.noroff.hvz.models.Users;

public class PlayerDto {

    private int playerId;
    private String name;
    private boolean isHuman;
    private String biteCode;
    private String userId;
    private Integer gameId;
    private Integer squadMemberId;

    public PlayerDto(Player player) {
        this.playerId = player.getId();
        this.name = player.getName();
        this.isHuman = player.isHuman();
        this.biteCode = player.getBiteCode();
        setUserId(player.getUser());
        setGameId(player.getGame());
        setSquadMemberId(player.getSquadMember());
    }

    private void setUserId(Users user) {
        if (user == null) {
            this.userId = null;
        } else {
            this.userId = user.getUserId();
        }
    }

    private void setGameId(Game game) {
        if (game == null) {
            this.gameId = null;
        } else {
            this.gameId = game.getGameId();
        }
    }

    private void setSquadMemberId(SquadMember squadMember) {
        if (squadMember == null) {
            this.squadMemberId = null;
        } else {
            this.squadMemberId = squadMember.getMemberId();
        }
    }

    public int getPlayerId() {
        return playerId;
    }

    public boolean isHuman() {
        return isHuman;
    }

    public String getBiteCode() {
        return biteCode;
    }

    public String getUserId() {
        return userId;
    }

    public Integer getGameId() {
        return gameId;
    }

    public Integer getSquadMemberId() {
        return squadMemberId;
    }

    public String getName() {
        return name;
    }
}
