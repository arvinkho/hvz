package no.noroff.hvz.models.dtos;

import no.noroff.hvz.models.Game;
import no.noroff.hvz.models.Mission;

public class MissionDto {

    private int missionId;
    private String name;
    private boolean isHumanVisible;
    private boolean isZombieVisible;
    private String description;
    private double nwLatitude;
    private double nwLongitude;
    private double seLatitude;
    private double seLongitude;
    private long startTime;
    private long endTime;
    private Integer gameId;

    public MissionDto (Mission mission) {
        this.missionId = mission.getMissionId();
        this.name = mission.getName();
        this.isHumanVisible = mission.isHumanVisible();
        this.isZombieVisible = mission.isZombieVisible();
        this.description = mission.getDescription();
        this.nwLatitude = mission.getNwLat();
        this.nwLongitude = mission.getNwLng();
        this.seLatitude = mission.getSeLat();
        this.seLongitude = mission.getSeLng();
        this.startTime = mission.getStartTime();
        this.endTime = mission.getEndTime();
        setGameId(mission.getGame());
    }

    private void setGameId(Game game) {
        if (game == null) {
            this.gameId = null;
        } else {
            this.gameId = game.getGameId();
        }
    }

    public int getMissionId() {
        return missionId;
    }

    public String getName() {
        return name;
    }

    public boolean isHumanVisible() {
        return isHumanVisible;
    }

    public boolean isZombieVisible() {
        return isZombieVisible;
    }

    public String getDescription() {
        return description;
    }

    public double getNwLatitude() {
        return nwLatitude;
    }

    public double getNwLongitude() {
        return nwLongitude;
    }

    public double getSeLatitude() {
        return seLatitude;
    }

    public double getSeLongitude() {
        return seLongitude;
    }

    public long getStartTime() {
        return startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public int getGameId() {
        return gameId;
    }
}
