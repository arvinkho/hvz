package no.noroff.hvz.models.dtos;

/**
 * Represents a Data Transfer Object used for the CREATION of a new squad checkin entity.
 */
public class SquadCheckinCreateDto {

    private long startTime;
    private long endTime;
    private double latitude;
    private double longitude;
    private int gameId;
    private int squadId;
    private int squadMemberId;

    public SquadCheckinCreateDto(long startTime, long endTime, double latitude,
                                 double longitude, int gameId, int squadId, int squadMemberId) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.latitude = latitude;
        this.longitude = longitude;
        this.gameId = gameId;
        this.squadId = squadId;
        this.squadMemberId = squadMemberId;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public int getSquadId() {
        return squadId;
    }

    public void setSquadId(int squadId) {
        this.squadId = squadId;
    }

    public int getSquadMemberId() {
        return squadMemberId;
    }

    public void setSquadMemberId(int squadMemberId) {
        this.squadMemberId = squadMemberId;
    }
}
