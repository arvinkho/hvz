package no.noroff.hvz.models.dtos;

import no.noroff.hvz.models.Game;
import no.noroff.hvz.models.Squad;
import no.noroff.hvz.models.SquadCheckin;
import no.noroff.hvz.models.SquadMember;

public class SquadCheckinDto {

    private int squadCheckinId;
    private long startTime;
    private long endTime;
    private double latitude;
    private double longitude;
    private Integer gameId;
    private Integer squadId;
    private Integer squadMemberId;

    public SquadCheckinDto (SquadCheckin squadCheckin) {
        this.squadCheckinId = squadCheckin.getSquadCheckinId();
        this.startTime = squadCheckin.getStartTime();
        this.endTime = squadCheckin.getEndTime();
        this.latitude = squadCheckin.getLat();
        this.longitude = squadCheckin.getLng();
        setGameId(squadCheckin.getGame());
        setSquadId(squadCheckin.getSquad());
        setSquadMemberId(squadCheckin.getSquadMember());
    }

    private void setGameId(Game game) {
        if (game == null) {
            this.gameId = null;
        } else {
            this.gameId = game.getGameId();
        }
    }

    private void setSquadId(Squad squad) {
        if (squad == null) {
            this.squadId = null;
        } else {
            this.squadId = squad.getSquadId();
        }
    }

    private void setSquadMemberId(SquadMember squadMember) {
        if (squadMember == null) {
            this.squadMemberId = null;
        } else {
            this.squadMemberId = squadMember.getMemberId();
        }
    }

    public int getSquadCheckinId() {
        return squadCheckinId;
    }

    public long getStartTime() {
        return startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public int getGameId() {
        return gameId;
    }

    public int getSquadId() {
        return squadId;
    }

    public int getSquadMemberId() {
        return squadMemberId;
    }
}
