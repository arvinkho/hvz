package no.noroff.hvz.models.dtos;

import java.time.Instant;

/**
 * Represents a Data Transfer Object used for the CREATION of a new kill entity.
 */
public class KillCreateDto {
    private long timeOfDeath;
    private String story;
    private Double latitude;
    private Double longitude;
    private String biteCode;
    private int gameId;
    private int killerId;
    private int victimId;

    public KillCreateDto(long timeOfDeath, String story, Double latitude,
                         Double longitude, String biteCode, int gameId, int killerId, int victimId) {
        this.timeOfDeath = timeOfDeath;
        this.story = story;
        this.latitude = latitude;
        this.longitude = longitude;
        this.biteCode = biteCode;
        this.gameId = gameId;
        this.killerId = killerId;
        this.victimId = victimId;
    }

    public long getTimeOfDeath() {
        return timeOfDeath;
    }

    public void setTimeOfDeath(long timeOfDeath) {
        this.timeOfDeath = timeOfDeath;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getBiteCode() {
        return biteCode;
    }

    public void setBiteCode(String biteCode) {
        this.biteCode = biteCode;
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public int getKillerId() {
        return killerId;
    }

    public void setKillerId(int killerId) {
        this.killerId = killerId;
    }

    public int getVictimId() {
        return victimId;
    }

    public void setVictimId(int victimId) {
        this.victimId = victimId;
    }
}
