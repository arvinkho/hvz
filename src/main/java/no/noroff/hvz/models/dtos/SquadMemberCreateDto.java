package no.noroff.hvz.models.dtos;

/**
 * Represents a Data Transfer Object used for the CREATION of a new squad member entity.
 */
public class SquadMemberCreateDto {

    private int gameId;
    // TODO RANK?
    private int squadId;
    private int playerId;

    public SquadMemberCreateDto(int gameId, int squadId, int playerId) {
        this.gameId = gameId;
        this.squadId = squadId;
        this.playerId = playerId;
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public int getSquadId() {
        return squadId;
    }

    public void setSquadId(int squadId) {
        this.squadId = squadId;
    }

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }
}
