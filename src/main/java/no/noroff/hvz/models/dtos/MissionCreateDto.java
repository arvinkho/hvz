package no.noroff.hvz.models.dtos;


/**
 * Represents a Data Transfer Object used for the CREATION of a new mission entity.
 */
public class MissionCreateDto {

    private String name;
    private boolean isHumanVisible;
    private boolean isZombieVisible;
    private String description;
    private double nwLatitude;
    private double nwLongitude;
    private double seLatitude;
    private double seLongitude;
    private long startTime;
    private long endTime;
    private int gameId;

    public MissionCreateDto(String name, boolean isHumanVisible, boolean isZombieVisible,
                            String description, double nwLatitude, double nwLongitude, double seLatitude,
                            double seLongitude, long startTime, long endTime, int gameId) {
        this.name = name;
        this.isHumanVisible = isHumanVisible;
        this.isZombieVisible = isZombieVisible;
        this.description = description;
        this.nwLatitude = nwLatitude;
        this.nwLongitude = nwLongitude;
        this.seLatitude = seLatitude;
        this.seLongitude = seLongitude;
        this.startTime = startTime;
        this.endTime = endTime;
        this.gameId = gameId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isHumanVisible() {
        return isHumanVisible;
    }

    public void setHumanVisible(boolean humanVisible) {
        this.isHumanVisible = humanVisible;
    }

    public boolean isZombieVisible() {
        return isZombieVisible;
    }

    public void setZombieVisible(boolean zombieVisible) {
        this.isZombieVisible = zombieVisible;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getNwLatitude() {
        return nwLatitude;
    }

    public void setNwLatitude(double nwLatitude) {
        this.nwLatitude = nwLatitude;
    }

    public double getNwLongitude() {
        return nwLongitude;
    }

    public void setNwLongitude(double nwLongitude) {
        this.nwLongitude = nwLongitude;
    }

    public double getSeLatitude() {
        return seLatitude;
    }

    public void setSeLatitude(double seLatitude) {
        this.seLatitude = seLatitude;
    }

    public double getSeLongitude() {
        return seLongitude;
    }

    public void setSeLongitude(double seLongitude) {
        this.seLongitude = seLongitude;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }
}
