package no.noroff.hvz.models.dtos;

import no.noroff.hvz.models.*;
import no.noroff.hvz.utils.GameState;

import java.util.List;
import java.util.stream.Collectors;

public class GameDto {

    private int gameId;
    private String name;
    private GameState gameState;
    private String description;
    private final String rules = """
            The game begins with one or more “Original Zombies” (OZ) or patient zero.
            The purpose of the OZ is to infect human players by tagging them. Once tagged,
            a human becomes a zombie for the remainder of the game.
            Human players are able to defend themselves against the zombie horde using Nerf
            weapons and clean, rolled-up socks which may be thrown to stun an unsuspecting
            zombie.
            """;
    private double nwLatitude;
    private double nwLongitude;
    private double seLatitude;
    private double seLongitude;
    private int playerCount;
    private List<Integer> missionIds;
    private List<Integer> playerIds;
    private List<Integer> killIds;
    private List<Integer> squadIds;
    private List<Integer> squadMemberIds;
    private List<Integer> squadCheckinIds;


    public GameDto(Game game) {
        gameId = game.getGameId();
        name = game.getName();
        gameState = game.getGameState();
        description = game.getDescription();
        nwLatitude = game.getNwLat();
        nwLongitude = game.getNwLng();
        seLatitude = game.getSeLat();
        seLongitude = game.getSeLng();
        setMissionIds(game.getMissions());
        setPlayerIds(game.getPlayers());
        setKillIds(game.getKill());
        setSquadIds(game.getSquads());
        setSquadMemberIds(game.getSquadMemberList());
        setSquadCheckinIds(game.getSquadCheckinList());
        setPlayerCount(game.getPlayers());
    }

    public int getGameId() {
        return gameId;
    }

    public String getName() {
        return name;
    }

    public GameState getGameState() {
        return gameState;
    }

    public String getDescription() {
        return description;
    }

    public String getRules() {
        return rules;
    }

    public double getNwLatitude() {
        return nwLatitude;
    }

    public double getNwLongitude() {
        return nwLongitude;
    }

    public double getSeLatitude() {
        return seLatitude;
    }

    public double getSeLongitude() {
        return seLongitude;
    }

    public int getPlayerCount() {
        return playerCount;
    }

    public List<Integer> getMissionIds() {
        return missionIds;
    }

    public List<Integer> getPlayerIds() {
        return playerIds;
    }

    public List<Integer> getKillIds() {
        return killIds;
    }

    public List<Integer> getSquadIds() {
        return squadIds;
    }

    public List<Integer> getSquadMemberIds() {
        return squadMemberIds;
    }

    public List<Integer> getSquadCheckinIds() {
        return squadCheckinIds;
    }

    private void setMissionIds(List<Mission> missions) {
        if (missions == null) {
            this.missionIds = null;
        } else {
            this.missionIds = missions.stream().map(Mission::getMissionId).collect(Collectors.toList());
        }
    }

    public void setPlayerIds(List<Player> players) {
        if (players == null) {
            this.playerIds = null;
        } else {
            this.playerIds = players.stream().map(Player::getId).collect(Collectors.toList());
        }
    }

    public void setKillIds(List<Kill> kills) {
        if (kills == null) {
            this.killIds = null;
        } else {
            this.killIds = kills.stream().map(Kill::getKillId).collect(Collectors.toList());
        }
    }

    public void setSquadIds(List<Squad> squads) {
        if (squads == null) {
            this.squadIds = null;
        } else {
            this.squadIds = squads.stream().map(Squad::getSquadId).collect(Collectors.toList());
        }
    }

    public void setSquadMemberIds(List<SquadMember> squadMembers) {
        if (squadMembers == null) {
            this.squadMemberIds = null;
        } else {
            this.squadMemberIds = squadMembers.stream().map(SquadMember::getMemberId).collect(Collectors.toList());
        }
    }

    public void setSquadCheckinIds(List<SquadCheckin> squadCheckins) {
        if (squadCheckins == null) {
            this.squadCheckinIds = null;
        } else {
            this.squadCheckinIds = squadCheckins.stream().map(SquadCheckin::getSquadCheckinId).collect(Collectors.toList());
        }
    }

    public void setPlayerCount(List<Player> playerList) {
        if (playerList == null)
            return;
        this.playerCount = playerList.size();
    }
}
