package no.noroff.hvz.models.dtos;


import no.noroff.hvz.utils.GameState;

/**
 * Represents a Data Transfer Object used for the CREATION of a new game entity.
 */
public class GameCreateDto {

    private String name;
    private String description;
    private double nwLatitude;
    private double nwLongitude;
    private double seLatitude;
    private double seLongitude;
    private GameState gameState;

    public GameCreateDto(String name, String description, double nwLatitude,
                         double nwLongitude, double seLatitude, double seLongitude, GameState gameState) {
        this.name = name;
        this.description = description;
        this.nwLatitude = nwLatitude;
        this.nwLongitude = nwLongitude;
        this.seLatitude = seLatitude;
        this.seLongitude = seLongitude;
        this.gameState = gameState;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getNwLatitude() {
        return nwLatitude;
    }

    public void setNwLatitude(double nwLatitude) {
        this.nwLatitude = nwLatitude;
    }

    public double getNwLongitude() {
        return nwLongitude;
    }

    public void setNwLongitude(double nwLongitude) {
        this.nwLongitude = nwLongitude;
    }

    public double getSeLatitude() {
        return seLatitude;
    }

    public void setSeLatitude(double seLatitude) {
        this.seLatitude = seLatitude;
    }

    public double getSeLongitude() {
        return seLongitude;
    }

    public void setSeLongitude(double seLongitude) {
        this.seLongitude = seLongitude;
    }

    public GameState getGameState() {
        return gameState;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }
}
