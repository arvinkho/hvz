package no.noroff.hvz.models.dtos;

import no.noroff.hvz.models.Player;

public class PlayerAdminDto extends PlayerDto {

    private boolean isPatientZero;

    // TODO: Add more fields? Find out what's "sensitive information" and add it to admin / move it from normal
    public PlayerAdminDto(Player player) {
        super(player);
        isPatientZero = player.isIsPatientZero();
    }

    public boolean isPatientZero() {
        return isPatientZero;
    }
}
