package no.noroff.hvz.models.dtos;

import no.noroff.hvz.models.Game;
import no.noroff.hvz.models.Player;
import no.noroff.hvz.models.Squad;
import no.noroff.hvz.models.SquadMember;

public class SquadMemberDto {
    private int squadMemberId;
    // private int rank; // TODO RANK?
    private Integer gameId;
    private Integer squadId;
    private Integer playerId;

    public SquadMemberDto(SquadMember squadMember) {
        this.squadMemberId = squadMember.getMemberId();
        setGameId(squadMember.getGame());
        setSquadId(squadMember.getSquad());
        setPlayerId(squadMember.getPlayer());
    }

    private void setGameId(Game game) {
        if (game == null) {
            this.gameId = null;
        } else {
            this.gameId = game.getGameId();
        }
    }

    private void setSquadId(Squad squad) {
        if (squad == null) {
            this.squadId = null;
        } else {
            this.squadId = squad.getSquadId();
        }
    }

    private void setPlayerId(Player player) {
        if (player == null) {
            this.playerId = null;
        } else {
            this.playerId = player.getId();
        }
    }

    public int getSquadMemberId() {
        return squadMemberId;
    }

    public int getGameId() {
        return gameId;
    }

    public int getSquadId() {
        return squadId;
    }

    public int getPlayerId() {
        return playerId;
    }
}
