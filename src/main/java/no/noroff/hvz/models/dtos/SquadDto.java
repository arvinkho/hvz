package no.noroff.hvz.models.dtos;

import no.noroff.hvz.models.Game;
import no.noroff.hvz.models.Squad;
import no.noroff.hvz.models.SquadCheckin;
import no.noroff.hvz.models.SquadMember;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SquadDto {
    private int squadId;
    private String name;
    private boolean isHuman;
    private Integer gameId;
    private List<Integer> squadMemberIds;
    private List<Integer> squadCheckinIds;
    private Integer humanCount = 0;
    private Integer memberCount = 0;

    public SquadDto (Squad squad) {
        this.squadId = squad.getSquadId();
        this.name = squad.getName();
        this.isHuman = squad.isHuman();
        setGameId(squad.getGame());
        setSquadMemberIds(squad.getSquadMemberList());
        setSquadCheckinIds(squad.getSquadCheckinList());
    }

    private void setGameId(Game game) {
        if (game == null) {
            this.gameId = null;
        } else {
            this.gameId = game.getGameId();
        }
    }

    private void setSquadMemberIds(List<SquadMember> squadMembers) {
        if (squadMembers == null) {
            this.squadMemberIds = null;
        } else {
            List<Integer> squadMemberList = new ArrayList<>();
            for (SquadMember squadMember : squadMembers) {
                if (squadMember.getPlayer().isHuman()) {
                    this.humanCount++;
                }
                this.memberCount++;
                squadMemberList.add(squadMember.getMemberId());
            }
            this.squadMemberIds = squadMemberList;
        }
    }

    private void setSquadCheckinIds(List<SquadCheckin> squadCheckins) {
        if (squadCheckins == null) {
            this.squadCheckinIds = null;
        } else {
            this.squadCheckinIds = squadCheckins.stream().map(SquadCheckin::getSquadCheckinId).collect(Collectors.toList());
        }
    }

    public int getSquadId() {
        return squadId;
    }

    public String getName() {
        return name;
    }

    public boolean isHuman() {
        return isHuman;
    }

    public int getGameId() {
        return gameId;
    }

    public List<Integer> getSquadMemberIds() {
        return squadMemberIds;
    }

    public List<Integer> getSquadCheckinIds() {
        return squadCheckinIds;
    }

    public Integer getHumanCount() {
        return humanCount;
    }

    public Integer getMemberCount() {
        return memberCount;
    }
}
