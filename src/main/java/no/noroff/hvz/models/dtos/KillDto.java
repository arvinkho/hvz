package no.noroff.hvz.models.dtos;

import no.noroff.hvz.models.Game;
import no.noroff.hvz.models.Kill;
import no.noroff.hvz.models.Player;

public class KillDto {

    private int killId;
    private long timeOfDeath;
    private String story;
    private Double latitude;
    private Double longitude;
    private Integer gameId;
    private Integer killerId;
    private Integer victimId;

    private String killerName;
    private String victimName;

    public KillDto (Kill kill) {
        this.killId = kill.getKillId();
        this.timeOfDeath = kill.getTimeOfDeath();
        this.story = kill.getStory();
        this.latitude = kill.getLat();
        this.longitude = kill.getLng();
        setGameId(kill.getGame());
        setKiller(kill.getKiller());
        setVictim(kill.getVictim());
    }

    private void setGameId(Game game) {
        if (game == null) {
            this.gameId = null;
        } else {
            this.gameId = game.getGameId();
        }
    }

    private void setKiller(Player killer) {
        if (killer == null) {
            this.killerId = null;
            killerName = null;
        } else {
            this.killerId = killer.getId();
            killerName = killer.getUser().getName();
        }
    }

    private void setVictim(Player victim) {
        if (victim == null) {
            this.victimId = null;
            victimName = null;
        } else {
            this.victimId = victim.getId();
            victimName = victim.getUser().getName();
        }
    }

    public int getKillId() {
        return killId;
    }

    public long getTimeOfDeath() {
        return timeOfDeath;
    }

    public String getStory() {
        return story;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Integer getGameId() {
        return gameId;
    }

    public Integer getKillerId() {
        return killerId;
    }

    public Integer getVictimId() {
        return victimId;
    }

    public String getKillerName() {
        return killerName;
    }

    public String getVictimName() {
        return victimName;
    }
}
