package no.noroff.hvz.models.dtos;

/**
 * Represents a Data Transfer Object used for the CREATION of a new squad entity.
 */
public class SquadCreateDto {
    private String name;
    private static final boolean isHuman = true; //TODO REMOVE
    private int gameId;

    public SquadCreateDto(String name, int gameId) {
        this.name = name;
        this.gameId = gameId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isHuman() {
        return isHuman;
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }
}
