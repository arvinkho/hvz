package no.noroff.hvz.models.dtos;

import no.noroff.hvz.models.Game;
import no.noroff.hvz.models.Player;
import no.noroff.hvz.models.Users;

public class UserDto {
    private String userId;
    private String name;
    private String email;
    private Integer playerId;
    //private Integer gameId;

    public UserDto (Users user) {
        this.userId = user.getUserId();
        this.name = user.getName();
        this.email = user.getEmail();
        setPlayerId(user.getPlayer());
        //setGameId(user.getGame());
    }

    public String getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public Integer getPlayerId() {
        return playerId;
    }

//    public Integer getGameId() {
//        return gameId;
//    }

    private void setPlayerId(Player player) {
        if (player == null) {
            this.playerId = null;
        } else {
            this.playerId = player.getId();
        }
    }

//    private void setGameId(Game game) {
//        if (game == null) {
//            this.gameId = null;
//        } else {
//            this.gameId = game.getGameId();
//        }
//    }
}
