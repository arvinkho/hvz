package no.noroff.hvz.models.dtos;

/**
 * Represents a Data Transfer Object used for the CREATION of a new player entity.
 */
public class PlayerCreateDto {

    private boolean isHuman;
    private String name;
    private String userId;
    private int gameId;

    public PlayerCreateDto(boolean isHuman, String name, String userId, int gameId) {
        this.isHuman = isHuman;
        this.name = name;
        this.userId = userId;
        this.gameId = gameId;
    }

    public boolean isHuman() {
        return isHuman;
    }

    public void setHuman(boolean human) {
        isHuman = human;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
