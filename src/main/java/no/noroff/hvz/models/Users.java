package no.noroff.hvz.models;

import javax.persistence.*;

@Entity
public class Users {

    @Id
    private String userId;
    private String name;
    private String email;

    //Relations:
    @OneToOne(mappedBy = "user")
    private Player player;

    @ManyToOne
    @JoinColumn(name = "game_id")
    private Game game;

    public Users(String userId, String name, String email, Player player, Game game) {
        this.userId = userId;
        this.name = name;
        this.email = email;
        this.player = player;
        this.game = game;
    }

    public Users(){}

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }


}
