package no.noroff.hvz.models;

import no.noroff.hvz.utils.GameState;

import javax.persistence.*;
import java.util.List;

@Entity
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer gameId;

    private String name;
    private GameState gameState;
    private String description;
    private double nwLat;
    private double nwLng;
    private double seLat;
    private double seLng;

    // Relations --
    @OneToMany(mappedBy = "game", cascade = CascadeType.REMOVE)
    private List<Users> users;

    @OneToMany(mappedBy = "game", cascade = CascadeType.REMOVE)
    private List<Mission> missions;

    @OneToMany(mappedBy = "game", cascade = CascadeType.REMOVE)
    private List<Player> players;

    @OneToMany(mappedBy = "game", cascade = CascadeType.REMOVE)
    private List<Kill> kill;

    @OneToMany(mappedBy = "game", cascade = CascadeType.REMOVE)
    private List<Squad> squads;

    @OneToMany(mappedBy = "game", cascade = CascadeType.REMOVE)
    private List<SquadMember> squadMemberList;

    @OneToMany(mappedBy = "game", cascade = CascadeType.REMOVE)
    private List<SquadCheckin> squadCheckinList;

    /**
     * TO DO!!
     * Relations til Chat må legges inn.
     *
     */

    public Game(Integer gameId, String name, GameState gameState, String description, double nwLat,
                double nwLng, double seLat, double seLng, List<Mission> missions) {
        this.gameId = gameId;
        this.name = name;
        this.gameState = gameState;
        this.description = description;
        this.nwLat = nwLat;
        this.nwLng = nwLng;
        this.seLat = seLat;
        this.seLng = seLng;
        this.missions = missions;
    }

    public Game() {
    }

    public Integer getGameId() {
        return gameId;
    }

    public void setGameId(Integer gameId) {
        this.gameId = gameId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GameState getGameState() {
        return gameState;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getNwLat() {
        return nwLat;
    }

    public void setNwLat(double nwLat) {
        this.nwLat = nwLat;
    }

    public double getNwLng() {
        return nwLng;
    }

    public void setNwLng(double nwLng) {
        this.nwLng = nwLng;
    }

    public double getSeLat() {
        return seLat;
    }

    public void setSeLat(double seLat) {
        this.seLat = seLat;
    }

    public double getSeLng() {
        return seLng;
    }

    public void setSeLng(double seLng) {
        this.seLng = seLng;
    }

    public List<Mission> getMissions() {
        return missions;
    }

    public void setMissions(List<Mission> missions) {
        this.missions = missions;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public List<Kill> getKill() {
        return kill;
    }

    public void setKill(List<Kill> kill) {
        this.kill = kill;
    }

    public List<Squad> getSquads() {
        return squads;
    }

    public void setSquads(List<Squad> squads) {
        this.squads = squads;
    }

    public List<SquadMember> getSquadMemberList() {
        return squadMemberList;
    }

    public void setSquadMemberList(List<SquadMember> squadMemberList) {
        this.squadMemberList = squadMemberList;
    }

    public List<SquadCheckin> getSquadCheckinList() {
        return squadCheckinList;
    }

    public void setSquadCheckinList(List<SquadCheckin> squadCheckinList) {
        this.squadCheckinList = squadCheckinList;
    }

    public List<Users> getUsers() {
        return users;
    }

    public void setUsers(List<Users> users) {
        this.users = users;
    }
}
