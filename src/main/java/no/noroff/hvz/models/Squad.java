package no.noroff.hvz.models;

import javax.persistence.*;
import java.util.List;

@Entity
public class Squad {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer squadId;

    private String name;
    private boolean isHuman;

    @ManyToOne
    @JoinColumn(name = "game_id")
    private Game game;

    @OneToMany(mappedBy = "squad", cascade = CascadeType.REMOVE)
    private List<SquadMember> squadMemberList;

    @OneToMany(mappedBy = "squad", cascade = CascadeType.REMOVE)
    private List<SquadCheckin> squadCheckinList;

    public Squad(Integer squadId, String name, boolean isHuman,
                 Game game, List<SquadMember> squadMemberList, List<SquadCheckin> squadCheckinList) {
        this.squadId = squadId;
        this.name = name;
        this.isHuman = isHuman;
        this.game = game;
        this.squadMemberList = squadMemberList;
        this.squadCheckinList = squadCheckinList;
    }

    public Squad() {
    }

    public Integer getSquadId() {
        return squadId;
    }

    public void setSquadId(Integer squad_id) {
        this.squadId = squad_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isHuman() {
        return isHuman;
    }

    public void setIsHuman(boolean is_human) {
        this.isHuman = is_human;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public List<SquadMember> getSquadMemberList() {
        return squadMemberList;
    }

    public void setSquadMemberList(List<SquadMember> squadMemberList) {
        this.squadMemberList = squadMemberList;
    }

    public List<SquadCheckin> getSquadCheckinList() {
        return squadCheckinList;
    }

    public void setSquadCheckinList(List<SquadCheckin> squadCheckinList) {
        this.squadCheckinList = squadCheckinList;
    }
}
