package no.noroff.hvz.models;

import javax.persistence.*;

@Entity
public class Mission {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer missionId;

    private String name;
    private boolean isHumanVisible;
    private boolean isZombieVisible;
    private String description;
    private double nwLat;
    private double nwLng;
    private double seLat;
    private double seLng;
    private Long startTime;
    private Long endTime;

    //Relations ---
    @ManyToOne
    @JoinColumn(name = "game_id")
    private Game game;

    public Mission(Integer missionId, String name, boolean isHumanVisible, boolean isZombieVisible,
                   String description,double nwLat, double nwLng, double seLat, double seLng,
                   Long startTime, Long endTime, Game game) {
        this.missionId = missionId;
        this.name = name;
        this.isHumanVisible = isHumanVisible;
        this.isZombieVisible = isZombieVisible;
        this.description = description;
        this.nwLat = nwLat;
        this.nwLng = nwLng;
        this.seLat = seLat;
        this.seLng = seLng;
        this.startTime = startTime;
        this.endTime = endTime;
        this.game = game;
    }

    public Mission(){}

    public Integer getMissionId() {
        return missionId;
    }

    public void setMissionId(Integer mission_id) {
        this.missionId = mission_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isHumanVisible() {
        return isHumanVisible;
    }

    public void setIsHumanVisible(boolean is_human_visible) {
        this.isHumanVisible = is_human_visible;
    }

    public boolean isZombieVisible() {
        return isZombieVisible;
    }

    public void setIsZombieVisible(boolean is_zombie_visible) {
        this.isZombieVisible = is_zombie_visible;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getNwLat() {
        return nwLat;
    }

    public void setNwLat(double nwLat) {
        this.nwLat = nwLat;
    }

    public double getNwLng() {
        return nwLng;
    }

    public void setNwLng(double nwLng) {
        this.nwLng = nwLng;
    }

    public double getSeLat() {
        return seLat;
    }

    public void setSeLat(double seLat) {
        this.seLat = seLat;
    }

    public double getSeLng() {
        return seLng;
    }

    public void setSeLng(double seLng) {
        this.seLng = seLng;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long start_time) {
        this.startTime = start_time;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long end_time) {
        this.endTime = end_time;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

}
