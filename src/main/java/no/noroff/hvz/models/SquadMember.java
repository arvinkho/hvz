package no.noroff.hvz.models;


import no.noroff.hvz.utils.SquadRank;

import javax.persistence.*;
import java.util.List;

@Entity
public class SquadMember {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer memberId;

    private SquadRank rank; // To do...

    //Relations
    @ManyToOne
    @JoinColumn(name = "game_id")
    private Game game;

    @ManyToOne
    @JoinColumn(name = "squad_id")
    private Squad squad;

    @OneToMany(mappedBy = "squadMember", cascade = CascadeType.REMOVE)
    private List<SquadCheckin> squadCheckinList;


    @OneToOne
    @JoinColumn(name = "player_id", referencedColumnName = "id")
    private Player player;



    public SquadMember(Integer memberId, SquadRank rank, Game game, Squad squad, Player player) {
        this.memberId = memberId;
        this.rank = rank;
        this.game = game;
        this.squad = squad;
        this.player = player;
    }

    public SquadMember() {
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer member_id) {
        this.memberId = member_id;
    }

    public SquadRank getRank() {
        return rank;
    }

    public void setRank(SquadRank rank) {
        this.rank = rank;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Squad getSquad() {
        return squad;
    }

    public void setSquad(Squad squad) {
        this.squad = squad;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public List<SquadCheckin> getSquadCheckinList() {
        return squadCheckinList;
    }

    public void setSquadCheckinList(List<SquadCheckin> squadCheckinList) {
        this.squadCheckinList = squadCheckinList;
    }
}
