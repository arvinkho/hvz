package no.noroff.hvz.mappers;

import no.noroff.hvz.models.SquadMember;
import no.noroff.hvz.models.dtos.SquadMemberCreateDto;
import no.noroff.hvz.models.dtos.SquadMemberDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class SquadMemberMapper implements Mapper<SquadMember, SquadMemberDto, SquadMemberCreateDto> {

    /**
     * Converts from a create-DTO to an entity (domain-object).
     *
     * @param squadMemberCreateDto create DTO
     * @return entity
     */
    @Override
    public SquadMember mapFromCreateDtoToEntity(SquadMemberCreateDto squadMemberCreateDto) {
        SquadMember squadMember = new SquadMember();

        return squadMember;
    }

    /**
     * Converts from entity (domain-object) to DTO
     *
     * @param squadMember entity
     * @return DTO
     */
    @Override
    public SquadMemberDto mapFromEntityToDto(SquadMember squadMember) {
        return new SquadMemberDto(squadMember);
    }

    /**
     * Converts from entity list (domain-object) to DTO list
     *
     * @param squadMembers entity list
     * @return DTO list
     */
    @Override
    public List<SquadMemberDto> mapFromEntityToDtoList(List<SquadMember> squadMembers) {
        List<SquadMemberDto> squadMemberDtos = new ArrayList<>();
        for (SquadMember squadMember : squadMembers) {
            squadMemberDtos.add(new SquadMemberDto(squadMember));
        }
        return squadMemberDtos;
    }

    /**
     * Converts from entity set (domain-object) to DTO set
     *
     * @param squadMembers entity set
     * @return DTO set
     */
    @Override
    public Set<SquadMemberDto> mapFromEntityToDtoSet(Set<SquadMember> squadMembers) {
        Set<SquadMemberDto> squadMemberDtos = new HashSet<>();
        for (SquadMember squadMember : squadMembers) {
            squadMemberDtos.add(new SquadMemberDto(squadMember));
        }
        return squadMemberDtos;
    }
}
