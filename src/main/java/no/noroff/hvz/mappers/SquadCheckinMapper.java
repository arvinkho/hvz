package no.noroff.hvz.mappers;

import no.noroff.hvz.models.SquadCheckin;
import no.noroff.hvz.models.dtos.SquadCheckinCreateDto;
import no.noroff.hvz.models.dtos.SquadCheckinDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class SquadCheckinMapper implements Mapper<SquadCheckin, SquadCheckinDto, SquadCheckinCreateDto> {

    /**
     * Converts from a create-DTO to an entity (domain-object).
     *
     * @param squadCheckinCreateDto create DTO
     * @return entity
     */
    @Override
    public SquadCheckin mapFromCreateDtoToEntity(SquadCheckinCreateDto squadCheckinCreateDto) {
        SquadCheckin squadCheckin = new SquadCheckin();
        squadCheckin.setStartTime(squadCheckinCreateDto.getStartTime());
        squadCheckin.setEndTime(squadCheckinCreateDto.getEndTime());
        squadCheckin.setLat(squadCheckinCreateDto.getLatitude());
        squadCheckin.setLng(squadCheckinCreateDto.getLongitude());
        return squadCheckin;
    }

    /**
     * Converts from entity (domain-object) to DTO
     *
     * @param squadCheckin entity
     * @return DTO
     */
    @Override
    public SquadCheckinDto mapFromEntityToDto(SquadCheckin squadCheckin) {
        return new SquadCheckinDto(squadCheckin);
    }

    /**
     * Converts from entity list (domain-object) to DTO list
     *
     * @param squadCheckins entity list
     * @return DTO list
     */
    @Override
    public List<SquadCheckinDto> mapFromEntityToDtoList(List<SquadCheckin> squadCheckins) {
        List<SquadCheckinDto> squadCheckinDtos = new ArrayList<>();
        for (SquadCheckin squadCheckin : squadCheckins) {
            squadCheckinDtos.add(new SquadCheckinDto(squadCheckin));
        }
        return squadCheckinDtos;
    }

    /**
     * Converts from entity set (domain-object) to DTO set
     *
     * @param squadCheckins entity set
     * @return DTO set
     */
    @Override
    public Set<SquadCheckinDto> mapFromEntityToDtoSet(Set<SquadCheckin> squadCheckins) {
        Set<SquadCheckinDto> squadCheckinDtos = new HashSet<>();
        for (SquadCheckin squadCheckin : squadCheckins) {
            squadCheckinDtos.add(new SquadCheckinDto(squadCheckin));
        }
        return squadCheckinDtos;
    }
}
