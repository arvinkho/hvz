package no.noroff.hvz.mappers;

import no.noroff.hvz.models.Mission;
import no.noroff.hvz.models.dtos.MissionCreateDto;
import no.noroff.hvz.models.dtos.MissionDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class MissionMapper implements Mapper<Mission, MissionDto, MissionCreateDto> {
    /**
     * Converts from a create-DTO to an entity (domain-object).
     * @param missionCreateDto create DTO
     * @return entity
     */
    @Override
    public Mission mapFromCreateDtoToEntity(MissionCreateDto missionCreateDto) {
        Mission mission = new Mission();
        mission.setName(missionCreateDto.getName());
        mission.setIsHumanVisible(missionCreateDto.isHumanVisible());
        mission.setIsZombieVisible(missionCreateDto.isZombieVisible());
        mission.setDescription(missionCreateDto.getDescription());
        mission.setNwLat(missionCreateDto.getNwLatitude());
        mission.setNwLng(missionCreateDto.getNwLongitude());
        mission.setSeLat(missionCreateDto.getSeLatitude());
        mission.setSeLng(missionCreateDto.getSeLongitude());
        mission.setStartTime(missionCreateDto.getStartTime());
        mission.setEndTime(missionCreateDto.getEndTime());
        //TODO
        //mission.setGameId(missionCreateDto.getGameId());
        return mission;
    }

    /**
     * Converts from entity (domain-object) to DTO
     * @param mission entity
     * @return DTO
     */
    @Override
    public MissionDto mapFromEntityToDto(Mission mission) {
        return new MissionDto(mission);
    }

    /**
     * Converts from entity list (domain-object) to DTO list
     * @param missions entity list
     * @return DTO list
     */
    @Override
    public List<MissionDto> mapFromEntityToDtoList(List<Mission> missions) {
        List<MissionDto> missionDtos = new ArrayList<>();
        for (Mission mission : missions) {
            missionDtos.add(new MissionDto(mission));
        }
        return missionDtos;
    }

    /**
     * Converts from entity set (domain-object) to DTO set
     * @param missions entity set
     * @return DTO set
     */
    @Override
    public Set<MissionDto> mapFromEntityToDtoSet(Set<Mission> missions) {
        Set<MissionDto> missionDtos = new HashSet<>();
        for (Mission mission : missions) {
            missionDtos.add(new MissionDto(mission));
        }
        return missionDtos;
    }
}
