package no.noroff.hvz.mappers;

import no.noroff.hvz.models.Users;
import no.noroff.hvz.models.dtos.UserCreateDto;
import no.noroff.hvz.models.dtos.UserDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserMapper implements Mapper<Users, UserDto, UserCreateDto> {


    /**
     * Converts from a create-DTO to an entity (domain-object).
     * @param userCreateDto create user DTO
     * @return user entity
     */
    @Override
    public Users mapFromCreateDtoToEntity(UserCreateDto userCreateDto) {
        Users user = new Users();
        user.setName(userCreateDto.getName());
        user.setEmail(userCreateDto.getEmail());
        return user;
    }

    /**
     * Converts from user entity (domain-object) to user DTO
     * @param user user entity
     * @return user DTO
     */
    @Override
    public UserDto mapFromEntityToDto(Users user) {
        return new UserDto(user);
    }

    /**
     * Converts from user entity list (domain-object) to user DTO list
     * @param users user entity list
     * @return user DTO list
     */
    @Override
    public List<UserDto> mapFromEntityToDtoList(List<Users> users) {
        List<UserDto> usersDtos = new ArrayList<>();
        for (Users user : users) {
            usersDtos.add(new UserDto(user));
        }
        return usersDtos;
    }

    /**
     * Converts from user entity set (domain-object) to user DTO set
     * @param users user entity set
     * @return user DTO set
     */
    @Override
    public Set<UserDto> mapFromEntityToDtoSet(Set<Users> users) {
        Set<UserDto> usersDtos = new HashSet<>();
        for (Users user : users) {
            usersDtos.add(new UserDto(user));
        }
        return usersDtos;
    }
}
