package no.noroff.hvz.mappers;

import no.noroff.hvz.models.Kill;
import no.noroff.hvz.models.dtos.KillCreateDto;
import no.noroff.hvz.models.dtos.KillDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class KillMapper implements Mapper<Kill, KillDto, KillCreateDto> {
    /**
     * Converts from a create-DTO to an entity (domain-object).
     * @param killCreateDto create DTO
     * @return entity
     */
    @Override
    public Kill mapFromCreateDtoToEntity(KillCreateDto killCreateDto) {
        Kill kill = new Kill();
        kill.setTimeOfDeath(killCreateDto.getTimeOfDeath());
        kill.setStory(killCreateDto.getStory());
        kill.setLat(killCreateDto.getLatitude());
        kill.setLng(killCreateDto.getLongitude());
        // TODO
//        kill.setGameId(killCreateDto.getGameId());
//        kill.setKillerId(killCreateDto.getKillerId());
//        kill.setVictimId(killCreateDto.getVictimId());
        return kill;
    }

    /**
     * Converts from entity (domain-object) to DTO
     * @param kill entity
     * @return DTO
     */
    @Override
    public KillDto mapFromEntityToDto(Kill kill) {
        return new KillDto(kill);
    }

    /**
     * Converts from entity list (domain-object) to DTO list
     * @param kills entity list
     * @return DTO list
     */
    @Override
    public List<KillDto> mapFromEntityToDtoList(List<Kill> kills) {
        List<KillDto> killDtos = new ArrayList<>();
        for (Kill kill : kills) {
            killDtos.add(new KillDto(kill));
        }
        return killDtos;
    }

    /**
     * Converts from entity set (domain-object) to DTO set
     * @param kills entity set
     * @return DTO set
     */
    @Override
    public Set<KillDto> mapFromEntityToDtoSet(Set<Kill> kills) {
        Set<KillDto> killDtos = new HashSet<>();
        for (Kill kill : kills) {
            killDtos.add(new KillDto(kill));
        }
        return killDtos;
    }
}
