package no.noroff.hvz.mappers;

import no.noroff.hvz.models.Squad;
import no.noroff.hvz.models.dtos.SquadCreateDto;
import no.noroff.hvz.models.dtos.SquadDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class SquadMapper implements Mapper<Squad, SquadDto, SquadCreateDto> {

    /**
     * Converts from a create-DTO to an entity (domain-object).
     *
     * @param squadCreateDto create DTO
     * @return entity
     */
    @Override
    public Squad mapFromCreateDtoToEntity(SquadCreateDto squadCreateDto) {
        Squad squad = new Squad();
        squad.setName(squadCreateDto.getName());
        squad.setIsHuman(squadCreateDto.isHuman());
        // TODO
        // squad.setGameId(squadCreateDto.getGameId());
        return squad;
    }

    /**
     * Converts from entity (domain-object) to DTO
     *
     * @param squad entity
     * @return DTO
     */
    @Override
    public SquadDto mapFromEntityToDto(Squad squad) {
        return new SquadDto(squad);
    }

    /**
     * Converts from entity list (domain-object) to DTO list
     *
     * @param squads
     * @return DTO list
     */
    @Override
    public List<SquadDto> mapFromEntityToDtoList(List<Squad> squads) {
        List<SquadDto> squadDtos = new ArrayList<>();
        for (Squad squad : squads) {
            squadDtos.add(new SquadDto(squad));
        }
        return squadDtos;
    }

    /**
     * Converts from entity set (domain-object) to DTO set
     *
     * @param squads
     * @return DTO set
     */
    @Override
    public Set<SquadDto> mapFromEntityToDtoSet(Set<Squad> squads) {
        Set<SquadDto> squadDtos = new HashSet<>();
        for (Squad squad : squads) {
            squadDtos.add(new SquadDto(squad));
        }
        return squadDtos;
    }
}
