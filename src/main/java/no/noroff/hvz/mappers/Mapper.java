package no.noroff.hvz.mappers;

import java.util.List;
import java.util.Set;

/**
 * This interface is implemented in mapper classes. It converts between entities (domain models),
 * DTOs and Create-DTOs.
 * @param <T> Entity/domain model
 * @param <D> DTO model
 * @param <C> Create-DTO model
 */
public interface Mapper<T, D, C> {
    /**
     * Converts from a create-DTO to an entity (domain-object).
     * @param c create DTO
     * @return entity
     */
    T mapFromCreateDtoToEntity(C c);

    /**
     * Converts from entity (domain-object) to DTO
     * @param t entity
     * @return DTO
     */
    D mapFromEntityToDto(T t);

    /**
     * Converts from entity list (domain-object) to DTO list
     * @param ts entity list
     * @return DTO list
     */
    List<D> mapFromEntityToDtoList(List<T> ts);

    /**
     * Converts from entity set (domain-object) to DTO set
     * @param ts entity set
     * @return DTO set
     */
    Set<D> mapFromEntityToDtoSet(Set<T> ts);
}
