package no.noroff.hvz.mappers;

import no.noroff.hvz.models.Game;
import no.noroff.hvz.models.dtos.GameCreateDto;
import no.noroff.hvz.models.dtos.GameDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class GameMapper implements Mapper<Game, GameDto, GameCreateDto> {
    /**
     * Converts from a create-DTO to an entity (domain-object).
     * @param gameCreateDto create DTO
     * @return entity
     */
    @Override
    public Game mapFromCreateDtoToEntity(GameCreateDto gameCreateDto) {
        Game game = new Game();
        game.setGameState(gameCreateDto.getGameState());
        game.setDescription(gameCreateDto.getDescription());
        game.setName(gameCreateDto.getName());
        game.setNwLat(gameCreateDto.getNwLatitude());
        game.setNwLng(gameCreateDto.getNwLongitude());
        game.setSeLat(gameCreateDto.getSeLatitude());
        game.setSeLng(gameCreateDto.getSeLongitude());
        return game;
    }

    /**
     * Converts from entity (domain-object) to DTO
     * @param game entity
     * @return DTO
     */
    @Override
    public GameDto mapFromEntityToDto(Game game) {
        return new GameDto(game);
    }

    /**
     * Converts from entity list (domain-object) to DTO list
     * @param games entity list
     * @return DTO list
     */
    @Override
    public List<GameDto> mapFromEntityToDtoList(List<Game> games) {
        List<GameDto> gameDtos = new ArrayList<>();

        for (Game game: games) {
            gameDtos.add(mapFromEntityToDto(game));
        }

        return gameDtos;
    }

    /**
     * Converts from entity set (domain-object) to DTO set
     * @param games entity set
     * @return DTO set
     */
    @Override
    public Set<GameDto> mapFromEntityToDtoSet(Set<Game> games) {
        return null;
    }
}
