package no.noroff.hvz.mappers;

import no.noroff.hvz.generator.BiteCodeGenerator;
import no.noroff.hvz.models.Player;
import no.noroff.hvz.models.dtos.PlayerAdminDto;
import no.noroff.hvz.models.dtos.PlayerCreateDto;
import no.noroff.hvz.models.dtos.PlayerDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class PlayerMapper implements Mapper<Player, PlayerDto, PlayerCreateDto> {
    /**
     * Converts from a create-DTO to an entity (domain-object).
     *
     * @param playerCreateDto create DTO
     * @return entity
     */
    @Override
    public Player mapFromCreateDtoToEntity(PlayerCreateDto playerCreateDto) {
        Player player = new Player();
        //player.setBiteCode(BiteCodeGenerator.generateBiteCode());
        //player.(playerCreateDto.getUserId());
        player.setHuman(playerCreateDto.isHuman());
        player.setName(playerCreateDto.getName());
        return player;
    }

    /**
     * Converts from entity (domain-object) to DTO
     *
     * @param player entity
     * @return DTO
     */
    @Override
    public PlayerDto mapFromEntityToDto(Player player) {
        return new PlayerDto(player);
    }

    /**
     * Converts from entity to admin DTO if isAdmin is true. Admin DTO contains more information than normal DTO
     *
     * @param player Entity
     * @param isAdmin Check if we want to return admin DTO or not.
     * @return PlayerDto if user isn't admin, PlayerAdminDto if user is admin.
     */
    public PlayerDto mapFromEntityToDto(Player player, boolean isAdmin) {
        if (isAdmin)
            return new PlayerAdminDto(player);
        return new PlayerDto(player);
    }

    /**
     * Converts from entity list (domain-object) to DTO list
     *
     * @param players entity list
     * @return DTO list
     */
    @Override
    public List<PlayerDto> mapFromEntityToDtoList(List<Player> players) {
        List<PlayerDto> playerDtos = new ArrayList<>();
        for (Player player : players) {
            playerDtos.add(new PlayerDto(player));
        }
        return playerDtos;
    }

    /**
     * Converts from entity list to DTO list
     *
     * @param players Entity list
     * @param isAdmin Check if we want normal or admin DTOs
     * @return DTO list
     */
    public List<PlayerDto> mapFromEntityToDtoList(List<Player> players, boolean isAdmin) {
        List<PlayerDto> playerDtos = new ArrayList<>();
        for (Player player: players) {
            if (isAdmin)
                playerDtos.add(new PlayerAdminDto(player));
            else
                playerDtos.add(new PlayerDto(player));
        }
        return playerDtos;
    }

    /**
     * Converts from entity set (domain-object) to DTO set
     *
     * @param players entity set
     * @return DTO set
     */
    @Override
    public Set<PlayerDto> mapFromEntityToDtoSet(Set<Player> players) {
        Set<PlayerDto> playerDtos = new HashSet<>();
        for (Player player : players) {
            playerDtos.add(new PlayerDto(player));
        }
        return playerDtos;
    }
}
