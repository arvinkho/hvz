package no.noroff.hvz.generator;

public enum Hundreds {
    DUCK,
    GOOSE,
    CAT,
    DOG,
    BIRD,
    HORSE,
    FROG,
    LION,
    TIGER,
    WOLF
}
