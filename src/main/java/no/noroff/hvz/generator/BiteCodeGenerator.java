package no.noroff.hvz.generator;

import java.util.Random;

public class BiteCodeGenerator {

    public static String generateBiteCode() {

        Random rand = new Random();
        int upperbound = 10000;
        int randomNumber = rand.nextInt(upperbound);
        StringBuilder enumBuilder = new StringBuilder();

        if (randomNumber < 10) {
            enumBuilder.append("000").append(randomNumber);
        }
        if (randomNumber > 9 && randomNumber < 100) {
            enumBuilder.append("00").append(randomNumber);
        }
        if (randomNumber > 99 && randomNumber < 1000) {
            enumBuilder.append("0").append(randomNumber);
        }
        if (randomNumber > 999){
            enumBuilder.append(randomNumber);
        }

        String input = enumBuilder.toString();

        StringBuilder builder = new StringBuilder();

        int one = Integer.parseInt(input.substring(0, 1));
        Ones ones = Ones.values()[one];
        builder.append(ones.toString()).append("-");

        int ten = Integer.parseInt(input.substring(1, 2));
        Tens tens = Tens.values()[ten];
        builder.append(tens.toString()).append("-");

        int hundred = Integer.parseInt(input.substring(2, 3));
        Hundreds hundreds = Hundreds.values()[hundred];
        builder.append(hundreds.toString()).append("-");

        int thousand = Integer.parseInt(input.substring(3,4));
        Thousands thousands = Thousands.values()[thousand];
        builder.append(thousands.toString());
        
        return builder.toString();
    }
}