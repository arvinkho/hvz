package no.noroff.hvz.generator;

public enum Ones {
    CHARMING,
    CRUEL,
    FANTASTIC,
    GENTLE,
    HUGE,
    PERFECT,
    ROUGH,
    SHARP,
    TASTY,
    ZEALOUS
}
