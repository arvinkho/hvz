package no.noroff.hvz.generator;

public enum Tens {
    RED,
    ORANGE,
    YELLOW,
    GREEN,
    BLUE,
    INDIGO,
    VIOLET,
    PURPLE,
    CYAN,
    MAGENTA
}
