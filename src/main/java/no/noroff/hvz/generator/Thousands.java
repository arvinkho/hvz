package no.noroff.hvz.generator;

public enum Thousands {
    SINGING,
    DANCING,
    BARKING,
    PARKING,
    BREAKDANCING,
    CODING,
    PLAYING,
    GRASSCUTTING,
    FIREFIGHTING,
    BINGEING
}
