INSERT INTO Game VALUES
    (1000, 'Test game', 0, 'Ultimate Game', 123, 123, 123, 123)
    ON CONFLICT (game_id) DO NOTHING ;

INSERT INTO Users VALUES
    ('6233382e6253630071cba5f2', 'Trym Ellingsen', 'Trym Ellingsen', 1000),
    ('ABC', null, null, 1000)
    ON CONFLICT (user_id) DO NOTHING;

INSERT INTO player VALUES
    (1000, 'ROUGH-VIOLET-GOOSE-GRASSCUTTING', false, true,'Trym', 1000, '6233382e6253630071cba5f2'),
    (2000, 'ABC', true, false, 'John Doe', 1000, 'ABC')
    ON CONFLICT (id) DO NOTHING;

INSERT INTO kill VALUES
    (1000, 123, 123, 'Someone just died', 123, 1000, 1000, 2000)
    ON CONFLICT (kill_id) DO NOTHING;

INSERT INTO squad VALUES
    (1000, true, 'Cannot touch this', 1000)
    ON CONFLICT (squad_id) DO NOTHING;

INSERT INTO squad_member VALUES
    (1000, 0, 1000, 2000, 1000)
    ON CONFLICT (member_id) DO NOTHING;

INSERT INTO squad_checkin VALUES
    (1000, 123, 123, 123, 123, 1000, 1000, 1000)
    ON CONFLICT (squad_checkin_id) DO NOTHING;
